import React, { useState } from "react";
import Confirm from "./Confirm";
import Popup from "./Popup";
import viewAccessIcon from "../resources/svg/view.svg";
import {
  hasPermissionTo,
  isCreatedFamily,
  getSelectedFamilyToVisit
} from "../tools/funcs";
import { familyConnectionRequest } from "../api";
import { strings } from "../constants";
// import { pages } from "../constants";
const ViewOtherAccess = ({ page }) => {
  const [show, setShow] = useState(false);
  const [confirmState, setConfirmState] = useState({
    show: false,
    title: "",
    content: "",
    onConfirm: () => {},
    onReject: () => {
      setConfirmState({ ...confirmState, show: false });
    }
  });
  const [popupState, setPopupState] = useState({
    show: false,
    title: "",
    content: "",
    onClose: () => {
      setPopupState({ ...popupState, show: false });
    }
  });
  if (hasPermissionTo()) {
    return <div></div>;
  }
  return (
    <div>
      <img
        className="viewAccessBtn"
        src={viewAccessIcon}
        alt="viewAccess"
        onClick={() => {
          if (isCreatedFamily()) {
            // alert("p1");
            setConfirmState({
              ...confirmState,
              show: true,
              content:
                strings.VIEW_FAMILY_NEED_PERMISSION_DO_YOU_WANT_TO_SEND_REQUEST,
              onConfirm: async () => {
                // request to server for permission
                try {
                  await familyConnectionRequest(getSelectedFamilyToVisit());
                  setConfirmState({ ...confirmState, show: false });
                } catch (error) {
                  console.error(error);
                }
              }
            });
          } else {
            setPopupState({
              ...popupState,
              show: true,
              content:
                strings.VIEW_FAMILY_PERMISSION_DENIED_NEEDS_YOUR_FAMILY_CREATOR_SEND_REQUEST
            });
          }
        }}
      />

      {confirmState.show && (
        <Confirm
          content={confirmState.content}
          onConfirm={confirmState.onConfirm}
          onReject={confirmState.onReject}
        />
      )}
      {popupState.show && (
        <Popup content={popupState.content} onClose={popupState.onClose} />
      )}
    </div>
  );
};

export default ViewOtherAccess;
