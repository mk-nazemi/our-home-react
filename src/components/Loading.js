import React, { useState, useEffect } from "react";
import * as animationData from "../resources/lottieFiles/upload.json";
import LottieRunner from "./LottieRunner";
import { UploadTransitions } from "../constants.js";
// import { pages } from "../constants";
const Loading = () => {
  console.log("Loading");
  const [transition, setTransition] = useState(null);
  useEffect(() => {
    const tr = UploadTransitions.uploadingPleaseWait;
    setTransition(tr);
  }, []);
  return (
    <div>
      <LottieRunner
        animationData={animationData}
        transition={transition}
        clearTransition={() => {
          setTransition(null);
        }}
        speed={1}
        width={"100%"}
        height={"100%"}
      />
    </div>
  );
};

export default Loading;
