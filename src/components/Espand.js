import React from "react";
import classNames from "classnames";
import EspandIcon from "../resources/svg/espand.svg";
import * as esfandData from "../resources/lottieFiles/Espand V04.json";
import LottieRunner from "./LottieRunner";
const Espand = ({ onClick }) => {
  const [cheshmzakhmTransition, setCheshmzakhmTransition] = React.useState(
    null
  );
  return (
    <span
      onClick={() => {
        setCheshmzakhmTransition({ from: 0, to: 38 });
        onClick();
      }}
    >
      <img
        src={EspandIcon}
        className="espandIcon"
        alt="moon"
        width="30%"
        height="30%"
      />
      <div
        className={classNames("cheshmzakhmEffect", {
          hideDisplay: cheshmzakhmTransition === null,
        })}
      >
        <LottieRunner
          className={classNames({
            hideDisplay: cheshmzakhmTransition === null,
          })}
          setState={(state) => {
            // this.setState(state);
          }}
          speed={1}
          animationData={esfandData}
          transition={cheshmzakhmTransition}
          clearTransition={() => {
            setCheshmzakhmTransition(null);
          }}
        />
      </div>
    </span>
  );
};

export default Espand;
