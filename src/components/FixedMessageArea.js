import React from "react";

const FixedMsgArea = ({ msg }) => {
  return <div className="fixedMsgArea">{msg}</div>;
};

export default FixedMsgArea;
