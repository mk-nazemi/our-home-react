import React, { useState, useEffect } from "react";
import checkBoxSelected from "../resources/svg/check-box.svg";
import checkBoxNotSelected from "../resources/svg/check-box-empty.svg";
import { pages } from "../constants";
const GiveFamilyPermission = ({ onChange }) => {
  console.log(pages);
  const [checkedItems, setCheckedItems] = useState({});
  //   useEffect(() => {
  //     onChange(checkedItems);
  //   }, [checkedItems, onChange]);
  //   console.log(checkedItems);
  return (
    <div className="GiveFamilyPermissionArea">
      <p>دسترسی مشاهده</p>
      {Object.entries(pages)
        .filter((entry) => entry[1].uploadFileType)
        .map((entry) => {
          const key = entry[0];
          const section = entry[1];
          return (
            <div className={`permission ${key}`}>
              <span>{section.title}</span>
              <img
                className="checkbox"
                src={
                  checkedItems[key] && checkedItems[key].view
                    ? checkBoxSelected
                    : checkBoxNotSelected
                }
                alt="checkbox"
                onClick={() => {
                  const cp = { ...checkedItems };
                  if (!cp[key]) cp[key] = {};
                  cp[key].view = !checkedItems[key];
                  setCheckedItems({ ...cp });
                  onChange(cp);
                }}
              />
            </div>
          );
        })}
    </div>
  );
};

export default GiveFamilyPermission;
