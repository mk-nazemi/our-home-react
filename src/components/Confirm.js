import React from "react";
import acceptSVG from "../resources/svg/accept-btn.svg";
import rejectSVG from "../resources/svg/reject-btn.svg";

const Confirm = ({ title, content, onConfirm, onReject }) => {
  return (
    <div className="confirm">
      <div className="content">{content}</div>
      <img
        src={acceptSVG}
        className="confirmOkBtn"
        alt="confirmOkBtn"
        onClick={() => {
          onConfirm();
        }}
      />
      <img
        src={rejectSVG}
        className="confirmRejectBtn"
        alt="confirmRejectBtn"
        onClick={() => {
          onReject();
        }}
      />
    </div>
  );
};

export default Confirm;
