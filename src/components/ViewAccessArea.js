import React, { useState } from "react";
// import UploadAccessArea from "./UploadAccessArea";
// import UploadAccessBtn from "../resources/svg/uploadAccessBtn.svg";
// import { pages } from "../constants";
// import bgBorder from "../resources/svg/bgBorder.svg";
import BackBtn from "./backBtn";
const members = [
  {
    name: "علی",
    id: 1
  },
  {
    name: "مریم",
    id: 2
  },
  {
    name: "رضا",
    id: 3
  }
];
const families = [
  {
    name: "family1",
    members
  },
  {
    name: "family2",
    members
  },
  {
    name: "family3",
    members
  }
];

const ViewAccessArea = ({ onClose, show }) => {
  const visibility = show ? "visible" : "hidden";
  return (
    <div className="viewAccessArea" style={{ visibility }}>
      <BackBtn onClick={() => onClose()} />
      {/* <img src={bgBorder} alt="searchArea" /> */}
      <div className="membersList">
        {families.map(family => (
          <div>
            {family.name}
            {family.members.map(member => (
              <div key={member.id}>
                <span>{member.name}</span>
                <span>
                  <input type="checkbox" />
                </span>
              </div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
};

export default ViewAccessArea;
