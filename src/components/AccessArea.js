import React, { useState, useEffect } from "react";
import UploadAccess from "./UploadAccessIcon";
import ViewAccess from "./ViewAccessIcon";
import ViewOtherAccess from "./ViewOtherAccessIcon";
import UploadFile from "./UploadFileIcon";
import { isItMyHome } from "../tools/funcs";
import SimpleLottieRunner from "./SimpleLottieRunner";
// import * as didemoonBazdidemoonAnimation from "../resources/lottieFiles/didemoon bazdidemoon.json";
import { accessArea } from "../constants";
// import { pages } from "../constants";
const AccessArea = ({ page, upload, gotoPage, onEditClicked }) => {
  const [showAnimation, setShowAnimation] = useState(false);
  const [rand, setRand] = useState(Math.random());

  console.log("rand", rand);
  useEffect(() => {
    setRand(Math.random());
  }, [page]);

  useEffect(() => {
    setTimeout(() => {
      setShowAnimation(true);
    }, 8000);
  }, [showAnimation]);
  const isMyHome = isItMyHome();
  if (!isMyHome)
    return (
      <div className="AccessArea">
        <ViewOtherAccess />
      </div>
    );
  return (
    <div className="AccessArea">
      {showAnimation && (
        <SimpleLottieRunner
          id={1}
          animationData={accessArea.animation.animationData}
          // animationData={didemoonBazdidemoonAnimation}
          onComplete={() => {
            console.log("accessArea.animation.animationData onComplete");
          }}
          onlyRender={false}
          isStopped={false}
          isPaused={false}
          loop={false}
          key={`accessBar${rand}`}
          autoplay
          from={0}
          to={accessArea.animation.to}
          speed={1}
          width="100%"
          height="100%"
        />
      )}
      <UploadAccess page={page} />
      <ViewAccess />
      <UploadFile page={page} upload={upload} gotoPage={gotoPage} />
      <div className="editBtn" onClick={onEditClicked}></div>
    </div>
  );
};

export default AccessArea;
