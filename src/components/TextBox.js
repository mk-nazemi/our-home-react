import React, { useState } from "react";
import TextBoxIcon from "../resources/svg/text-box.svg";
// import { pages } from "../constants";
const TextBox = ({ type = "text", value, onChange }) => {
  return (
    <div className="textboxArea">
      <img className="TextBoxSVG" src={TextBoxIcon} alt="TextBox" />
      <input
        type={type}
        className="textboxInput"
        value={value}
        onChange={onChange}
      />
    </div>
  );
};

export default TextBox;
