import React from "react";
import Lottie from "react-lottie";

export default class LottieRunner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      runningTransition: false,
      transition: null,
      direction: 1,
      autoplay: false,
      loop: false,
      speed: 1,
      offset: null,
      isStopped: false,
      isPaused: true
    };
    this.lottie = React.createRef();
  }

  componentDidMount() {
    // this.lottie.current.anim.goToAndStop(840, true);
    // this.props.setState({ currentStep: "home" });

    console.log("LottieRunner componentDidMount");
    if (this.props.startFrame != null) {
      this.lottie.current.anim.goToAndStop(this.props.startFrame, true);
    }
    // this.lottie.current.anim.goToAndStop(this.props.from, true);
  }
  componentDidUpdate() {
    // console.log("LottieRunner componentDidUpdate");
    // console.log(this.props.transition, this.state.runningTransition);
    if (
      this.props.transition != null &&
      this.state.runningTransition === false
      // (this.props.transition && this.props.transition.forceRun)
    ) {
      console.log("new transition");
      this.lottie.current.anim.goToAndStop(this.props.transition.from, true);
      this.setState({
        isPaused: false,
        runningTransition: true,
        transition: {
          ...this.props.transition
        },
        direction: this.props.transition.direction || 1,
        speed:
          (this.props.transition && this.props.transition.speed) ||
          this.props.speed ||
          // this.state.speed
          1
      });
    }

    if (this.props.gotoFrame != null) {
      this.lottie.current.anim.goToAndStop(this.props.gotoFrame, true);
      this.setState({ isPaused: true });
      this.props.setState({ gotoFrame: null });
    }
  }
  render() {
    const defaultOptions = {
      loop: false,
      autoplay: false,
      animationData: this.props.animationData.default,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice"
      }
    };
    // console.log(this.lottie.current);
    // console.log("LottieRunner props", this.props);
    return (
      <div>
        <Lottie
          direction={this.state.direction}
          width={this.props.width}
          height={this.props.height}
          ref={this.lottie}
          isClickToPauseDisabled={true}
          options={defaultOptions}
          isStopped={this.state.isStopped}
          isPaused={this.state.isPaused}
          speed={this.state.speed}
          eventListeners={[
            {
              eventName: "enterFrame",
              callback: data => {
                // console.log(data);
                if (this.state.isPaused || !this.state.runningTransition)
                  return;

                // console.log(this.state.transition);
                if (this.state.transition && this.state.transition.callbacks) {
                  for (
                    let index = 0;
                    index < this.state.transition.callbacks.length;
                    index++
                  ) {
                    const callback = this.state.transition.callbacks[index];
                    // console.log("callback is ", callback);
                    if (
                      !callback.processed &&
                      ((this.state.direction === 1 &&
                        callback.frame <= data.currentTime) ||
                        (this.state.direction === -1 &&
                          callback.frame >= data.currentTime))
                    ) {
                      console.log("running callback", callback);
                      callback.callback();

                      callback.processed = true;
                      const newCallbacks = this.state.transition.callbacks;
                      newCallbacks[index].processed = true;

                      this.setState({
                        transition: {
                          ...this.state.transition,
                          callback: newCallbacks
                        }
                      });
                    }
                  }
                }
                // console.log(
                //   this.state.direction,
                //   this.state.transition.from,
                //   this.state.transition.to,
                //   data.currentTime
                // );
                if (
                  (this.state.direction === 1 &&
                    this.state.transition.to <= data.currentTime) ||
                  (this.state.direction === -1 &&
                    this.state.transition.to >= data.currentTime)
                ) {
                  console.log("in enterFrame", this.props);
                  this.props.clearTransition();
                  this.setState({
                    isPaused: true,
                    runningTransition: false
                  });
                }
              }
            },
            {
              eventName: "complete",
              callback: data => {
                console.log("complete", data);
                this.props.clearTransition();
                this.setState({
                  isPaused: true,
                  runningTransition: false
                });
              }
            },
            {
              eventName: "LoopComplete",
              callback: data => console.log("LoopComplete", data)
            },
            {
              eventName: "SegmentStart",
              callback: data => console.log("SegmentStart", data)
            }
          ]}
        />
      </div>
    );
  }
}
