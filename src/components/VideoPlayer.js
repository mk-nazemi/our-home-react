import React, { useEffect } from "react";
import { Player, BigPlayButton, ControlBar, Shortcut } from "video-react";
import Seeker from "./Seeker";
import "../styles/video-react.css";
import LottieRunner from "./LottieRunner";

import * as playPauseLottieData from "../resources/lottieFiles/playpause.json";
import * as moonData from "../resources/lottieFiles/Moon V04.json";
import * as esfandData from "../resources/lottieFiles/Espand V04.json";

import LikeIcon from "../resources/svg/moon.svg";
import EspandIcon from "../resources/svg/espand.svg";
import ExpandIcon from "../resources/svg/extend button.svg";
import DownloadIcon from "../resources/svg/download.svg";
import viewIcon from "../resources/svg/view.svg";
import TVIcon from "../resources/svg/tv.svg";
import { likeFile, esfandFile, seenFile } from "../api";

import classNames from "classnames";
import { downloadURI, prettyNum, getVideoThumb } from "../tools/funcs";
const VideoPlayer = ({ videos }) => {
  const playerRef = React.createRef();

  const [playerVisible, setPlayerVisible] = React.useState(false);
  const [enableControls, setEnableControls] = React.useState(false);
  const [videoDuration, setVideoDuration] = React.useState(0);
  const [selectedVideo, setSelectedVideo] = React.useState(
    videos && videos[0] ? videos[0] : {}
  );
  const [currentTime, setCurrentTime] = React.useState(0);
  const [playPauseTransition, setPlayPauseTransition] = React.useState(null);
  const [moonTransition, setMoonTransition] = React.useState(null);
  const [cheshmzakhmTransition, setCheshmzakhmTransition] = React.useState(
    null
  );

  useEffect(() => {
    // if (videos.length) setSelectedVideo(videos[0]);
  }, [videos]);
  // console.log(selectedVideo);
  React.useEffect(() => {
    playerRef.current.subscribeToStateChange((state, prevState) => {
      // console.log(state, prevState);
      setEnableControls(state.isFullscreen);
      setVideoDuration(state.duration);
      setCurrentTime(state.currentTime);
      // console.log(state.paused, prevState.paused);
      if (state.paused !== prevState.paused) {
        if (!state.paused) {
          setPlayerVisible(true);
          setPlayPauseTransition({ from: 56, to: 120 });
        } else {
          // setPlayerVisible(false);
          setPlayPauseTransition({ from: 0, to: 56 });
        }
      }
    });
  });

  return (
    <div className="videoplayer">
      <div
        className="videoPlayerPlayBtn"
        onClick={() => {
          console.log("clicked");
          const { player } = playerRef.current.getState();
          if (player.paused) {
            playerRef.current.play();
            setPlayerVisible(true);
            setPlayPauseTransition({ from: 56, to: 120 });
            seenFile(selectedVideo.id);
          } else {
            // setPlayerVisible(false);
            playerRef.current.pause();
            setPlayPauseTransition({ from: 0, to: 56 });
          }
        }}
      >
        <LottieRunner
          setState={(state) => {
            // this.setState(state);
          }}
          animationData={playPauseLottieData}
          transition={playPauseTransition}
          clearTransition={() => {
            setPlayPauseTransition(null);
          }}
          startFrame={56}
        />
      </div>

      <Seeker
        setTime={(newTime) => {
          console.log("setTime called");
          playerRef.current.seek(newTime);
        }}
        duration={videoDuration}
        currentTime={currentTime}
      />

      <div className="tvIcon">
        <img className="tvIcon" src={TVIcon} alt="download icon" />
      </div>
      <div
        className="expandBtn"
        onClick={() => {
          playerRef.current.toggleFullscreen();
        }}
      >
        <img className="expandIcon" src={ExpandIcon} alt="expand" />
      </div>

      <div className="actionsArea">
        <div
          className={classNames("likeEffect", {
            hideDisplay: moonTransition === null,
          })}
        >
          <LottieRunner
            setState={(state) => {
              // this.setState(state);
            }}
            speed={3}
            width={"50%"}
            height={"50%"}
            animationData={moonData}
            transition={moonTransition}
            startFrame={0}
            clearTransition={() => {
              setMoonTransition(null);
            }}
          />
        </div>

        <div
          className={classNames("cheshmzakhmEffect", {
            hideDisplay: cheshmzakhmTransition === null,
          })}
        >
          <LottieRunner
            className={classNames({
              hideDisplay: cheshmzakhmTransition === null,
            })}
            setState={(state) => {
              // this.setState(state);
            }}
            // speed={1}
            width={"50%"}
            height={"50%"}
            animationData={esfandData}
            transition={cheshmzakhmTransition}
            clearTransition={() => {
              setCheshmzakhmTransition(null);
            }}
          />
        </div>

        <span
          onClick={async () => {
            console.log("clicked");
            setMoonTransition({ from: 0, to: 130 });
            await likeFile(selectedVideo.id);
          }}
        >
          <span className="moonCount">
            {selectedVideo && prettyNum(selectedVideo.likeCount)}
          </span>
          <img src={LikeIcon} className="moonIcon" alt="moon" />
        </span>
        <span
          onClick={() => {
            console.log("clicked");
            setCheshmzakhmTransition({ from: 0, to: 38 });
            esfandFile(selectedVideo.id);
          }}
        >
          <img src={EspandIcon} className="espandIcon" alt="moon" />
        </span>

        <span
          onClick={() => {
            console.log("download clicked");
            if (typeof androidApp === "undefined")
              downloadURI(selectedVideo.link);
            // setCheshmzakhmTransition({ from: 0, to: 38 });
            // esfandFile(selectedVideo.id);
          }}
        >
          <img
            className="downloadIcon"
            src={DownloadIcon}
            alt="download icon"
          />
        </span>
      </div>

      <div
        id="video-player"
        className={classNames({
          "player-visible": playerVisible,
        })}
      >
        <Player
          fluid={false}
          height="100%"
          playsInline
          ref={playerRef}
          src={
            selectedVideo &&
            selectedVideo.file &&
            selectedVideo.file.downloadUrl
          }
        >
          <ControlBar disabled={!enableControls} />
          <BigPlayButton disabled />
          <Shortcut clickable={false} />
        </Player>
      </div>

      <div className="videos-list">
        {videos &&
          videos.map((video) => (
            <div className="box">
              <img
                onClick={(e) => {
                  // setPlayerSrc(e.currentTarget.getAttribute("data-videoUrl"));
                  console.log("clicked");
                  setSelectedVideo(video);
                  playerRef.current.play();
                  seenFile(video.id);
                }}
                src={getVideoThumb(video.file)}
                onError={(e) => {
                  e.target.src = viewIcon;
                }}
                alt="ben"
              />
            </div>
          ))}
        {/* {videos[1] && (
          <div className="box">
            <img
              onClick={e => {
                console.log("clciked");
                setSelectedVideo(videos[1]);
                playerRef.current.play();
                seenFile(videos[1].id);
              }}
              src={videos[1].thumbnail}
              alt="ben"
            />
          <</div>
        )}
        {videos[2] && (
          <div className="box">
            <imggetImgThumb
              onClick={e => {
                setSelectedVideo(videos[2]);
                playerRef.current.play();
                seenFile(videos[2].id);
              }}
              src={videos[2].thumbnail}
              alt="ben"
            />
          </div>
        )}
        {videos[3] && (
          <div className="box">
            <img
              onClick={e => {
                setSelectedVideo(videos[3]);
                playerRef.current.play();
                seenFile(videos[3].id);
              }}
              src={videos[3].thumbnail}
              alt="ben"
            />
          </div>
        )} */}
      </div>
    </div>
  );
};

export default VideoPlayer;
