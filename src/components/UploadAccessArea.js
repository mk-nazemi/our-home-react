import React, { useState, useEffect } from "react";
import { PERMISSION_LEVELS } from "../constants";
import {
  getSelectedFamilyToVisitInfo,
  checkFamilyPermission,
} from "../tools/funcs";
import { strings } from "../constants";
import uploadTitleSVG from "../resources/svg/upload-title.svg";
import Popup from "./Popup";
import BackBtn from "./backBtn";
import AcceptBtn from "./AcceptBtn";
import { grantAccess, getFamilyPermissions } from "../api";

const info = getSelectedFamilyToVisitInfo();
const members = info.members ? info.members : [];

const UploadAccessArea = ({ onClose, show, page }) => {
  const [permissions, setPermissions] = useState([]);
  const [upPermissions, setUpPermissions] = useState(new Set());
  const [showPopup, setShowPopup] = useState(false);

  async function getPermissions() {
    const p = await getFamilyPermissions();
    setPermissions(p);

    const UPs = new Set();
    const pageName = page.name;
    if (p[pageName]) {
      p[pageName].Upload.map((user) => {
        UPs.add(user._id);
      });
    }
    console.log("UPs", UPs);
    setUpPermissions(UPs);
  }
  useEffect(() => {
    getPermissions();
  }, []);

  console.log(upPermissions);
  //   const [show, setShow] = useState(false);
  //   return <div UploadAccessArea=''>ok we are here</div>;
  const visibility = show ? "visible" : "hidden";
  return (
    <div className="uploadAccessArea" style={{ visibility }}>
      {showPopup && (
        <Popup
          content={strings.DONE_SUCCESSFULLY}
          onClose={() => {
            setShowPopup(false);
          }}
        />
      )}

      <BackBtn onClick={() => onClose()} />
      <div className="title">
        <img src={uploadTitleSVG} alt="whoCanUpload" />
      </div>
      {/* <img src={bgBorder} alt="searchArea" /> */}
      <div className="membersList">
        {members.map((member) => (
          <div className="member" key={member.memberId}>
            <span>
              {member.firstname} {member.lastname}
            </span>
            <span>
              <input
                type="checkbox"
                checked={upPermissions.has(member.memberId)}
                onClick={(e) => {
                  // console.log(e.target.value);
                  console.log(member);
                  const cp = new Set(upPermissions);
                  const id = member.memberId;
                  if (cp.has(id)) cp.delete(id);
                  else cp.add(id);

                  setUpPermissions(cp);
                }}
              />
            </span>
          </div>
        ))}
      </div>
      <div className="btnArea">
        <AcceptBtn
          width="30%"
          height="30%"
          onClick={async () => {
            const APs = members.map((member) => {
              return new Promise(async (resolve, reject) => {
                try {
                  const { memberId } = member;
                  console.log(
                    checkFamilyPermission(
                      permissions,
                      page.name,
                      PERMISSION_LEVELS.VIEW,
                      memberId
                    )
                  );

                  console.log(
                    checkFamilyPermission(
                      permissions,
                      page.name,
                      PERMISSION_LEVELS.UPLOAD,
                      memberId
                    )
                  );

                  if (
                    upPermissions.has(memberId) &&
                    !checkFamilyPermission(
                      permissions,
                      page.name,
                      PERMISSION_LEVELS.UPLOAD,
                      memberId
                    )
                  ) {
                    // user grant access
                    await grantAccess(memberId, {
                      [page.name]: PERMISSION_LEVELS.UPLOAD,
                    });
                    resolve();
                  } else if (
                    !upPermissions.has(memberId) &&
                    checkFamilyPermission(
                      permissions,
                      page.name,
                      PERMISSION_LEVELS.UPLOAD,
                      memberId
                    )
                  ) {
                    // user had access but now upload permission is not available, switch to prev permission
                    if (
                      checkFamilyPermission(
                        permissions,
                        page.name,
                        PERMISSION_LEVELS.VIEW,
                        memberId
                      )
                    ) {
                      await grantAccess(memberId, {
                        [page.name]: PERMISSION_LEVELS.VIEW,
                      });
                    } else {
                      await grantAccess(memberId, {
                        [page.name]: null,
                      });
                    }
                  }
                  resolve();
                } catch (error) {
                  reject(error);
                }
              });
            });
            await Promise.all(APs);
            await getPermissions();
            setShowPopup(true);
          }}
        />
      </div>
    </div>
  );
};

export default UploadAccessArea;
