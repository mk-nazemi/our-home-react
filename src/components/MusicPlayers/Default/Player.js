import React from "react";
import backBtnSVG from "../resources/svg/back_btn.svg";
import SimpleLottieRunner from "../../SimpleLottieRunner";

const Player = ({}) => {
  return (
    <SimpleLottieRunner
      animationData={animationData}
      onComplete={() => {}}
      onlyRender={false}
      isPaused={false}
      isStopped={false}
      autoplay
      loop={false}
      speed={1}
      width="100%"
      height="100%"
    />
  );
};

export default Player;
