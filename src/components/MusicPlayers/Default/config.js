import * as defaultPlayerAnimationData from "./resources/lottieFiles/components/upload/uploadFailed.json";
export const defaultPlayerAnimations = {
  player: {
    animationData: defaultPlayerAnimationData,
  },
};
