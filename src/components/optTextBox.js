import React, { useEffect } from "react";
import phone from "phone";
import PhoneCode from "react-phone-code";
// import { correctPhoneNumber } from "../tools/funcs";

const OTPTextBox = ({
  className,
  code,
  setCode,
  phoneNumber,
  setPhoneNumber,
}) => {
  // useEffect(() => {
  //   setCode(code || "+98");
  // }, [code, setCode]);
  return (
    <div className={`otpTextBox ${className}`}>
      <input
        type="number"
        id="otp-phone"
        maxLength="12"
        autoComplete="off"
        value={phoneNumber}
        onChange={(e) => {
          const phoneVal = e.target.value;
          const validCode = code || "+98";
          const newPhoneWithCode = phone(`${validCode}${phoneVal}`);
          // console.log(newPhoneWithCode);
          // console.log(code);
          if (newPhoneWithCode.length === 2) {
            // console.log(
            //   newPhoneWithCode,
            //   validCode,
            //   newPhoneWithCode[0].replace(validCode, "")
            // );
            setCode(validCode);
            setPhoneNumber(newPhoneWithCode[0].replace(validCode, ""));
          } else {
            setPhoneNumber(phoneVal);
          }
        }}
      />
      <PhoneCode
        onSelect={(code) => setCode(code)}
        showFirst={["IR", "US"]}
        // defaultValue="انتخاب کشور"
        className="countryCode"
      />
    </div>
  );
};

export default OTPTextBox;
