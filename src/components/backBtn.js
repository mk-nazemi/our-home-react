import React from "react";
import backBtnSVG from "../resources/svg/back_btn.svg";

const BackBtn = ({ onClick }) => {
  return (
    <img
      src={backBtnSVG}
      alt="back btn"
      className="arrowBackBtn"
      onClick={() => onClick()}
    />
  );
};

export default BackBtn;
