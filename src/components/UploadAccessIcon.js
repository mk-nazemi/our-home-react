import React, { useState } from "react";
import UploadAccessArea from "./UploadAccessArea";

// import uploadAccessBtn from "../resources/svg/upload-access.svg";
const UploadAccessIcon = ({ page }) => {
  const [show, setShow] = useState(false);
  

  return (
    <div>
      {/* <img
        className="uploadAccessBtn"
        src={uploadAccessBtn}
        alt="uploadAccess"
        onClick={() => {
          console.log(1);
          // setShow(true);
        }}
      /> */}
      <div
        className="uploadAccessBtn"
        onClick={async () => {
          // console.log(1);
          setShow(true);
        }}
      ></div>
      <UploadAccessArea
        page={page}
        onClose={() => setShow(false)}
        show={show}
      />
    </div>
  );
};

export default UploadAccessIcon;
