import React, { useState } from "react";
import SubmitButtonIcon from "../resources/svg/accept.svg";
// import { pages } from "../constants";
const SubmitButton = ({ onClick }) => {
  return (
    <img
      className="SubmitButtonSVG"
      src={SubmitButtonIcon}
      alt="SubmitButton"
      onClick={onClick}
    />
  );
};

export default SubmitButton;
