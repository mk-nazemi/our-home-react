import React from "react";
import classNames from "classnames";
import LottieRunner from "./LottieRunner";
import * as moonData from "../resources/lottieFiles/Moon V04.json";
import LikeIcon from "../resources/svg/moon.svg";
import { prettyNum } from "../tools/funcs";

const Moon = ({ onClick, likeCount }) => {
  const [moonTransition, setMoonTransition] = React.useState(null);
  return (
    <span
      className="moon"
      onClick={async () => {
        console.log("clicked");
        setMoonTransition({ from: 0, to: 130 });
        onClick();
      }}
    >
      <span className="moonCount">{likeCount && prettyNum(likeCount)}</span>
      <img
        src={LikeIcon}
        className="moonIcon"
        alt="moon"
        width="100%"
        height="100%"
      />
      <div
        className={classNames("likeEffect", {
          hideDisplay: moonTransition === null,
        })}
      >
        <LottieRunner
          setState={(state) => {
            // this.setState(state);
          }}
          speed={3}
          width="100%"
          height="100%"
          animationData={moonData}
          transition={moonTransition}
          clearTransition={() => {
            setMoonTransition(null);
          }}
        />
      </div>
    </span>
  );
};

export default Moon;
