import React from "react";
import acceptSVG from "../resources/svg/accept-btn.svg";

const Popup = ({ title, content, onClose }) => {
  return (
    <div className="popup">
      <div className="content">{content}</div>
      <img
        src={acceptSVG}
        className="popupOkBtn"
        alt="popupOkBtn"
        onClick={() => {
          onClose();
        }}
      />
    </div>
  );
};

export default Popup;
