import React, { useState } from "react";
import InvitationIconSVG from "../resources/svg/invite.svg";
import InviteOthers from "../pages/inviteOthers/inviteOthers";
import { pages } from "../constants";
const InviteOthersIcon = ({ gotoPage }) => {
  const [show, setShow] = useState(false);
  return (
    <div>
      <img
        className="inviteOthersIcon"
        src={InvitationIconSVG}
        alt="inviteOthersIcon"
        onClick={() => {
          window.scrollTo(0, 0);
          setShow(true);
        }}
      />
      {show && <InviteOthers onClose={() => setShow(false)} />}
    </div>
  );
};

export default InviteOthersIcon;
