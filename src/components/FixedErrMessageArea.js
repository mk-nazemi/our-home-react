import React from "react";
import FixedMsgArea from "./FixedMessageArea";
import { strings } from "../constants";
const FixedErrMsgArea = ({ msg }) => {
  switch (msg) {
    case "Invalid Pin!":
      msg = "کد وارد شده اشتباه است";
      break;
    case "ALREADY REQUESTED!":
      msg = "درخواست کد شما ارسال شده است. لطفا صبر کنید";
      break;
    case "INVALID REFERRAL!":
      msg = strings.INVALID_REFERRAL_OTP_ERROR;
      break;
    case "":
      msg = "";
      break;
    default:
      msg = "خطایی رخ داده دوباره امتحان کنید";
  }
  return <FixedMsgArea msg={msg} />;
};

export default FixedErrMsgArea;
