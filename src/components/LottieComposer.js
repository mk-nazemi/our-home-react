import React, { useEffect } from "react";
import SimpleLottieRunner from "./SimpleLottieRunner";
export function LottieComposer({ timeline }) {
  useEffect(() => {}, [timeline]);
  return (
    <SimpleLottieRunner
      animationData={this.state.animationData}
      key={this.state.lottieRunnerKey}
      isPaused
      isStopped
      autoplay
      loop={this.state.loop}
      speed={this.state.speed}
      width={this.state.width}
      height={this.state.height}
    />
  );
}
