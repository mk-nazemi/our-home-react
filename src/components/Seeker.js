import React from "react";
import Draggable from "react-draggable";
import defaultSeekerLine from "../resources/svg/seeker line.svg";
import SeekerPoint from "../resources/svg/seeker.svg";
const Seeker = ({
  setTime,
  duration,
  seekerLine = defaultSeekerLine,
  noSeekerLine = false,
  currentTime = 0
}) => {
  //   console.log("noSeekerLine", noSeekerLine);
  const seekerLineRef = React.createRef();
  const seekerPointRef = React.createRef();

  const [seekerXPosition, setSeekerXPosition] = React.useState(false);

  React.useEffect(() => {
    // console.log(currentTime, duration, seekerLineRef.current.offsetWidth);

    let newX = 0;
    if (duration)
      newX = (currentTime / duration) * seekerLineRef.current.offsetWidth;
    // console.log(newX);
    setSeekerXPosition(newX);
  }, [currentTime, duration, seekerLineRef]);
  return (
    <div
      className="seekerLine"
      ref={seekerLineRef}
      onMouseMove={e => console.log(e)}
      onClick={e => {
        console.log("seekerLine clicked", e);
        if (duration === 0) return;
        let x = e.pageX - e.currentTarget.offsetLeft;
        let ratio = x / seekerLineRef.current.offsetWidth;
        let newSecond = ratio * duration;
        setTime(newSecond);
        setSeekerXPosition(x);
      }}
    >
      {noSeekerLine && <div className="virtualSeekerLine"></div>}
      {!noSeekerLine && (
        <img className="seekerLineIcon" src={seekerLine} alt="seekerline" />
      )}
      <Draggable
        axis="x"
        handle=".handle"
        defaultPosition={{ x: 0, y: 0 }}
        position={{ x: seekerXPosition, y: 0 }}
        // position={null}
        grid={[25, 25]}
        bounds="parent"
        scale={1}
        onStart={(event, data) => console.log("draggable onStart", data)}
        onDrag={(event, data) => {
          // console.log("draggable onDrag", data);
          setSeekerXPosition({ x: data.lastX, y: data.lastY });
        }}
        onStop={(event, data) => {
          console.log("draggable onStop", data);
          if (duration === 0) return;
          // setSeekerPosition({ x: 10 });
          let ratio =
            (data.lastX + seekerPointRef.current.offsetWidth) /
            seekerLineRef.current.offsetWidth;

          let newSecond = ratio * duration;
          setTime(newSecond);
          setSeekerXPosition({ x: data.lastX, y: data.lastY });
        }}
      >
        <div className="seekerPoint" ref={seekerPointRef}>
          <img
            className="seekerPointIcon handle"
            src={SeekerPoint}
            alt="seekerline"
          />
        </div>
      </Draggable>
    </div>
  );
};

export default Seeker;
