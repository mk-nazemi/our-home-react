import React from "react";
import SearchIconSVG from "../resources/svg/searchbar.svg";
import { pages } from "../constants";
const SearchIcon = ({ gotoPage }) => {
  return (
    <img
      className="searchBarIcon"
      src={SearchIconSVG}
      alt="searchIcon"
      onClick={() => {
        gotoPage({ page: pages.search });
      }}
    />
  );
};

export default SearchIcon;
