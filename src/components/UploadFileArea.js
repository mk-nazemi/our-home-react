import React, { useState, useEffect } from "react";
import axios from "axios";
import jimp from "jimp";
import { loadProgressBar } from "axios-progress-bar";
import { strings, VIEW_ACCESSES, UploadTransitions2 } from "../constants";
import LottieRunner from "./LottieRunner";
// import FileUploadProgress from "react-fileupload-progress";
import * as animationData from "../resources/lottieFiles/upload.json";
import { FileManager, FileUploader } from "reactjs-file-uploader";
import { getUploadUrl, uploadDone } from "../api/index";
import boxSVG from "../resources/svg/box.svg";
import viewAccessSVG from "../resources/svg/viewAccess.svg";

// import uploadTitleSVG from "../resources/svg/upload-title.svg";
// import * as playPauseLottieData from "../resources/lottieFiles/playpause.json";
import BackBtn from "./backBtn";
loadProgressBar();
const UploadFileArea = ({ onClose, upload }) => {
  const [uploadPercent, setUploadPercent] = useState(0);
  const [transition, setTransition] = useState(null);
  const [gotoFrame, setGotoFrame] = useState(null);
  const [page, setPage] = useState("");
  const [files, setFiles] = useState([]);
  const [thumb, setThumb] = useState(null);
  const [uploadLinks, setUploadLinks] = useState([]);
  const [displayCaptureVideo, setDisplayCaptureVideo] = useState(false);
  // const [uploadStarted, setUploadStarted] = useState([]);
  const [filesUploadStatus, setFilesUploadStatus] = useState([]);
  const [accessType, setAccessType] = useState("private");
  const [additionalData, setAdditionalData] = useState({
    caption: "",
  });

  console.log(upload);

  const [uploadLinksReady, setUploadLinksReady] = useState(false);

  function onFileChange(files) {
    setFiles(Array.from(files));
    console.log(files);
    const file = files[0];
    if (upload.fileType === "Video") {
      const video = document.getElementById("videoPlayer");
      setDisplayCaptureVideo(true);
      video.src = URL.createObjectURL(file);
      capture();
    }
  }

  function capture() {
    const video = document.getElementById("videoPlayer");
    const imgCanvas = document.getElementById("imgCanvas");
    imgCanvas.width = video.videoWidth;
    imgCanvas.height = video.videoHeight;
    const context = imgCanvas.getContext("2d");
    context.drawImage(video, 0, 0);
    setThumb(imgCanvas.toDataURL());
  }

  useEffect(() => {
    const tr = UploadTransitions2["start-to-upload"];
    tr.callbacks = [
      {
        frame: tr.to,
        callback: () => {
          setPage("upload");
        },
      },
    ];
    setTransition(tr);
  }, []);

  useEffect(() => {
    async function getUploadURLS() {
      const upLinks = [];
      for (let index = 0; index < files.length; index++) {
        const file = files[index];
        console.log("getting upload url");
        // const uploadURL = await new Promise(resolve =>
        //   setTimeout(resolve, 3000)
        // );
        console.log(upload);
        let getUploadLinkRes;
        if (!upload.fileType) console.error("fileType is required");
        try {
          getUploadLinkRes = await getUploadUrl(file.name, upload.fileType);
        } catch (error) {
          console.log("error in getUploadUrl");
          console.error(error);
          const cp = [...filesUploadStatus];
          cp[index] = "error";
          setFilesUploadStatus(cp);
          setGotoFrame(1566);
          setPage("uploadError");
        }
        console.log("getting upload url done");
        upLinks.push(getUploadLinkRes);
      }
      setUploadLinks(upLinks);
      setUploadLinksReady(true);
    }
    if (page === "uploading") {
      console.log("here");
      setTransition(UploadTransitions2.uploadingPleaseWait);
      getUploadURLS();
    }
  }, [page]);

  function uploadFiles(files) {
    // const result = [];
    // for (let index = 0; index < files.length; index++) {
    //   //   const uploadURL = await new Promise(resolve => setTimeout(resolve, 2000));
    //   const r = uploadFile(files[index]);
    //   result.push(r);
    // }
    // console.log("result", result);
    // return result;

    return files.map(uploadFile);
  }

  function errorInUpload(e, index) {
    console.log("error in upload", e);
    const cp = [...filesUploadStatus];
    cp[index] = "error";
    setFilesUploadStatus(cp);
    setGotoFrame(1566);
    setPage("uploadError");
  }
  // console.log(files, uploadFiles, typeof uploadFiles);
  function uploadFile(file, index) {
    if (filesUploadStatus[index] === undefined) {
      // const formData = new FormData();
      // formData.append("file", file);
      axios
        .put(uploadLinks[index].uploadUrl, file, {
          onUploadProgress: (progressEvent) => {
            const totalLength = progressEvent.lengthComputable
              ? progressEvent.total
              : progressEvent.target.getResponseHeader("content-length") ||
                progressEvent.target.getResponseHeader(
                  "x-decompressed-content-length"
                );
            console.log("onUploadProgress", totalLength);
            if (totalLength !== null) {
              const percent = Math.floor(
                (progressEvent.loaded * 100) / totalLength
              );
              console.log(
                Math.floor((progressEvent.loaded * 100) / totalLength)
              );
              setUploadPercent(percent);
            }
          },
          headers: {
            "Content-Type": file.type,
          },
        })
        .then(async () => {
          try {
            // image file in base64 string
            let videoThumbnail;
            if (thumb) {
              const image = await jimp.read(thumb);
              image.resize(200, 200);
              videoThumbnail = await image.getBase64Async(jimp.MIME_JPEG);
            }

            await uploadDone(uploadLinks[index].fileId, {
              ...upload,
              type: accessType,
              metadata: additionalData,
              videoThumbnail,
            });
            console.log("upload done");

            const cp = [...filesUploadStatus];
            cp[index] = "done";
            setFilesUploadStatus(cp);
            setGotoFrame(1251);
            setPage("uploadDone");
          } catch (error) {
            errorInUpload(error, index);
          }
        })
        .catch((e) => {
          errorInUpload(e, index);
        });
    }

    if (filesUploadStatus[index] !== "uploading") {
      const cp = [...filesUploadStatus];
      cp[index] = "uploading";
      setFilesUploadStatus(cp);
    }

    return <div>{filesUploadStatus[index]}</div>;
    // return (
    //   <FileUploader
    //     autoUpload
    //     // onUploadReady={async() => {
    //     //     const uploadURL = await new Promise(resolve => setTimeout(resolve, 2000));
    //     // }}
    //     key={file.key}
    //     file={file}
    //     method="PUT"
    //     url={uploadLinks[index].uploadUrl}
    //     onDownloadStart={async () => {
    //       await uploadDone(uploadLinks[index].fileId, {
    //         ...upload,
    //         metadata: additionalData
    //       });
    //       const cp = { ...filesUploadStatus };
    //       cp[index] = "uploadComplete";
    //       setFilesUploadStatus(cp);
    //       // setTransition(UploadTransitions.uploadCompleteTransition);
    //       setTransition(null);
    //       setGotoFrame(1251);
    //     }}
    //     // formData={file}
    //     // formData={{
    //     // file
    //     // upload_preset: "public",
    //     // tags: "vanilla"
    //     // }}
    //   >
    //     {fileProgress(index)}
    //   </FileUploader>
    // );
  }

  function fileProgress(index) {
    return function fileProgressHandler({
      /*
    References to the Event objects.
    Initial state is null and each propert gets assigned on Event.
     */
      uploadReady,
      uploadStart,
      uploadProgress,
      uploadComplete,
      downloadStart,
      downloadProgress,
      downloadComplete,
      error,
      abort,
      timeout,

      /*
    The sequential state of the request
    enum {
        uploadReady, uploadStart, uploadProgress, uploadComplete, downloadStart
        downloadStart, downloadProgress, downloadComplete
    }
     */
      requestState,

      /*
    Function references to start / abort request
      */
      startUpload,
      abortRequest,

      /*
    Request Object reference (XMLHttpReqeust)
     */
      request,

      /*
    Response text Object (JSON)
     */
      response,

      /*
    Data of the file being uploaded (if readData props is true)
     */
      fileData,
    }) {
      console.log(index, requestState);
      // console.log(
      //   uploadReady,
      //   uploadStart,
      //   uploadProgress,
      //   uploadComplete,
      //   downloadStart,
      //   downloadProgress,
      //   downloadComplete,
      //   error,
      //   abort,
      //   timeout
      // );
      // console.log(request, response);
      // switch (requestState) {
      //   case "uploadReady":
      //     // startUpload();
      //     break;
      //   case "uploadComplete":
      //     if (filesUploadStatus[index] !== requestState) {
      //       console.log("uploadComplete1");
      //       const cp = { ...filesUploadStatus };
      //       cp[index] = requestState;
      //       setFilesUploadStatus(cp);
      //       console.log("uploadComplete2", cp);
      //       // setTransition(null);
      //       setTransition(UploadTransitions.uploadCompleteTransition);
      //       console.log("uploadComplete3");
      //     }
      //     break;
      //   default:
      //     break;
      // }
      // if (error || timeout) {
      //   console.log("errrrrrr1");
      //   if (filesUploadStatus[index] !== requestState) {
      //     const cp = { ...filesUploadStatus };
      //     cp[index] = requestState;
      //     setFilesUploadStatus(cp);
      //     //   setTransition(null);
      //     setTransition(UploadTransitions.uploadFailedTransition);
      //   }
      // }
      return (
        <div>
          {/* {fileData && <img src={fileData} width={200} alt="Preview" />} */}
          {/* {startUpload && <button onClick={startUpload}>Upload File</button>} */}
          {/* {requestState && requestState} */}
          {/* {uploadComplete &&
          setTransition(UploadTransitions.uploadCompleteTransition)}
        {(error || timeout) &&
          setTransition(UploadTransitions.uploadFailedTransition)} */}
          {/* setTransition(null); */}
        </div>
      );
    };
  }

  // console.log(transition);
  return (
    <div className="Area uploadFileArea">
      <BackBtn
        onClick={() => {
          if (upload.onClosed) upload.onClosed();
          onClose();
        }}
      />
      <LottieRunner
        animationData={animationData}
        transition={transition}
        gotoFrame={gotoFrame}
        setState={() => setGotoFrame(null)}
        clearTransition={() => {
          setTransition(null);
        }}
        speed={1}
        width={"100%"}
        height={"100%"}
      />
      {/* <img src={uploadTitleSVG} alt="upload-title" /> */}
      {page === "upload" && (
        <div className="upload-page">
          <input
            type="file"
            accept={upload.validTypes}
            className="fileInput"
            onChange={(event) => {
              onFileChange(event.target.files);
            }}
          />
          <button
            className="startUpload"
            onClick={() => {
              if (upload.fileType === "Video" && thumb === null) return;
              setPage("uploading");
            }}
          ></button>
          <div className="additionalData">
            <div className="whoCanSee">
              <img src={viewAccessSVG} alt="textarea" />
              <select
                value={accessType}
                onChange={(e) => {
                  setAccessType(e.target.value);
                }}
              >
                {VIEW_ACCESSES.map((accessType) => (
                  <option value={accessType.value}>{accessType.label}</option>
                ))}
              </select>
            </div>
            <div className="metadata">
              <span className="field">متن دلخواه</span>
              <img src={boxSVG} alt="textarea" />
              <textarea
                value={additionalData.caption}
                onChange={(e) => {
                  setAdditionalData({
                    ...additionalData,
                    caption: e.target.value,
                  });
                }}
                className="text"
              ></textarea>
            </div>

            <div
              id="captureVideoArea"
              style={{ display: displayCaptureVideo ? "block" : "none" }}
            >
              <video
                id="videoPlayer"
                src=""
                style={{ width: "100%", height: "100%" }}
                controls
                onLoadedData={() => {
                  console.log("onLoadedData");
                  capture();
                }}
              />
              <button
                id="capture"
                onClick={() => {
                  capture();
                }}
              >
                {strings.captureImage}
              </button>
              <canvas style={{ width: "100%" }} id="imgCanvas"></canvas>
            </div>
          </div>
        </div>
      )}

      <div
        className="uploading"
        style={{ visibility: page === "uploading" ? "visible" : "hidden" }}
      >
        <div
          className="uploadPercent"
          style={{
            position: "absolute",
            top: "47%",
            left: "36%",
            fontSize: "3em",
          }}
        >
          {uploadPercent}
        </div>
        {uploadLinksReady && (
          <FileManager files={files}>{uploadFiles}</FileManager>
        )}
      </div>

      {/* <button
        onClick={() => {
          setTransition(UploadTransitions.uploadingPleaseWait);
        }}
      >
        start
      </button> */}
    </div>
  );
};

export default UploadFileArea;
