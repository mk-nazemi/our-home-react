import React, { useState } from "react";
import UploadFileArea from "./UploadFileArea";
import Popup from "./Popup";
import uploadFileIcon from "../resources/svg/uploadFile.svg";
import { hasPermission, getSelectedFamilyToVisit } from "../tools/funcs";
import { strings, PERMISSION_LEVELS } from "../constants";
// import { pages } from "../constants";

// import { pages } from "../constants";
const UploadFile = ({ page, upload, gotoPage }) => {
  const [show, setShow] = useState(false);
  const [showPopup, setShowPopup] = useState(false);
  return (
    <div>
      {showPopup && (
        <Popup
          content={strings.UPLOAD_PERMISSION_DENIED}
          onClose={() => {
            setShowPopup(false);
          }}
        />
      )}

      {/* <img
        className="uploadFileBtn"
        src={uploadFileIcon}
        alt="uploadFile"
        onClick={() => {
          console.log(1);
          setShow(true);
        }}
      /> */}
      <div
        className="uploadFileBtn"
        onClick={() => {
          // console.log(1);
          // gotoPage({ page: pages.ahangamoon });
          // console.log(page);
          console.log(
            hasPermission(
              getSelectedFamilyToVisit(),
              page.name,
              PERMISSION_LEVELS.UPLOAD
            )
          );

          if (
            !hasPermission(
              getSelectedFamilyToVisit(),
              page.name,
              PERMISSION_LEVELS.UPLOAD
            )
          ) {
            return setShowPopup(true);
          }
          setShow(true);
        }}
      ></div>
      {show && (
        <UploadFileArea
          upload={upload}
          onClose={() => {
            typeof upload.onClose === "function" && upload.onClose();
            setShow(false);
          }}
        />
      )}
    </div>
  );
};

export default UploadFile;
