import React, { useState } from "react";
import InvitationIconSVG from "../resources/svg/invite-list.svg";
import { pages } from "../constants";
import InvitationRequests from "../pages/invitationRequests/invitationRequests";
const InvitationRequestsIcon = ({ gotoPage }) => {
  const [show, setShow] = useState(false);
  return (
    <div>
      <img
        className="InvitationRequestsIcon"
        src={InvitationIconSVG}
        alt="InvitationRequestsIcon"
        onClick={() => {
          window.scrollTo(0, 0);
          setShow(true);
        }}
      />
      {show && <InvitationRequests onClose={() => setShow(false)} />}
    </div>
  );
};

export default InvitationRequestsIcon;
