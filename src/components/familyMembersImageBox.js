import React, { useState, useEffect } from "react";
import { USER_PROFILE_PIC_URL_TEMPLATE } from "../config";
import SimpleLottieRunner from "./SimpleLottieRunner";
import { familyMembersTransitions } from "../constants";
import UploadProfileImage from "./UploadProfileImage";
import {
  getDoorBellConfig,
  setDoorBellConfig as setDoorBellConfigAPI,
} from "../api";
import {
  getSelectedFamilyToVisit,
  hasAdminAccessTo,
  generateMemberImage,
} from "../tools/funcs";

const FamilyMembersImageBox = ({ familyInfo, uploadDone }) => {
  // const familyInfo = {
  //   members: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  // };
  const [doorbellConfig, setDoorbellConfig] = useState({});
  async function setup() {
    const familyId = getSelectedFamilyToVisit();
    const config = (await getDoorBellConfig(familyId)) || {};
    console.log(config);
    setDoorbellConfig(config);
  }
  useEffect(() => {
    setup();
  }, []);
  const [showUpload, setShowUpload] = useState(false);
  const [uploadBoxPosition, setUploadBoxPosition] = useState(0);
  let animationData;
  if (familyInfo && familyInfo.members && familyInfo.members.length)
    animationData = familyMembersTransitions[familyInfo.members.length - 1];

  return (
    <div className="FamilyMembersImageBox">
      <div className="familyMembersImage">
        <div className="imageProfilesAnimation">
          {animationData && (
            <SimpleLottieRunner
              animationData={animationData}
              onComplete={() => {}}
              onlyRender={false}
              isPaused={false}
              isStopped={false}
              autoplay
              loop={false}
              speed={1}
              width="100%"
              height="100%"
            />
          )}
        </div>
        {/* doorbellConfig.memberProfileImages[member.memberId].order */}
        <div className="images">
          {familyInfo.members && (
            <div className={`numOfMembers_${familyInfo.members.length}`}>
              {familyInfo.members.map((member, index) => {
                return (
                  <div
                    className={`box box${index + 1}`}
                    onClick={() => {
                      console.log(index);
                      if (!hasAdminAccessTo(getSelectedFamilyToVisit())) return;
                      setShowUpload(true);
                      setUploadBoxPosition(index);
                    }}
                  >
                    {doorbellConfig.memberProfileImages &&
                      doorbellConfig.memberProfileImages[index] && (
                        <img
                          src={generateMemberImage(
                            familyInfo.familyId,
                            doorbellConfig.memberProfileImages[index]
                          )}
                          alt={`${member.firstname}`}
                        />
                      )}
                  </div>
                );
              })}
            </div>
          )}
        </div>
      </div>
      {showUpload && (
        <UploadProfileImage
          onClose={() => setShowUpload(false)}
          familyMembers={familyInfo.members}
          uploadDone={(memberId) => {
            try {
              const cp = { ...doorbellConfig };
              if (!cp.memberProfileImages || !cp.memberProfileImages.length) {
                cp.memberProfileImages = [];
              }
              cp.memberProfileImages.forEach((item, index) => {
                if (item === memberId) cp.memberProfileImages.splice(index, 1);
              });
              cp.memberProfileImages[uploadBoxPosition] = memberId;
              setDoorbellConfig(cp);
              setDoorBellConfigAPI(cp);
            } catch (error) {
              console.error(error);
            }
          }}
        />
      )}
    </div>
  );
};

export default FamilyMembersImageBox;
