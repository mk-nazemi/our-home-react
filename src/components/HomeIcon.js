import React from "react";
import InvitationIconSVG from "../resources/svg/home.svg";
import { pages } from "../constants";
import { gotoMyHome } from "../tools/funcs";
const HomeIcon = ({ gotoPage }) => {
  return (
    <img
      className="HomeIcon"
      src={InvitationIconSVG}
      alt="HomeIcon"
      onClick={() => {
        gotoMyHome();
        gotoPage({ page: pages.doorBell });
      }}
    />
  );
};

export default HomeIcon;
