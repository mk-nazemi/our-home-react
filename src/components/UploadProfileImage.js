import React, { useState, useEffect } from "react";
import { UploadTransitions } from "../constants";
import SimpleLottieRunner from "./SimpleLottieRunner";
import jimp from "jimp";
import { setMemberProfile } from "../api/index";

import BackBtn from "./backBtn";
const UploadProfileImage = ({ onClose, familyMembers, uploadDone }) => {
  const [uploadPercent, setUploadPercent] = useState(0);
  const [fileUploadStatus, setFileUploadStatus] = useState(null);
  const [file, setFile] = useState(null);
  const [activePage, setActivePage] = useState("selectFile");
  const [animation, setAnimation] = useState(null);
  const [loop, setLoop] = useState(false);
  const [selectedMember, setSelectedMember] = useState(
    familyMembers[0].memberId
  );

  function setPage(page) {
    setActivePage(page);
    switch (page) {
      case "selectFile":
        setAnimation(UploadTransitions.selectFile.animationData);
        break;
      case "uploading":
        setAnimation(UploadTransitions.uploading.animationData);
        setLoop(true);
        uploadFile(file);
        break;
      case "uploadDone":
        typeof uploadDone === "function" && uploadDone(selectedMember);
        setLoop(false);
        setAnimation(UploadTransitions.succeed.animationData);
        break;
      case "uploadFailed":
        setLoop(false);
        setAnimation(UploadTransitions.failed.animationData);
        break;
      default:
        break;
    }
  }

  useEffect(() => {
    setPage("selectFile");
    window.scrollTo(0, 0);
  }, []);

  function uploadFile(file) {
    console.log("uploadFile", file);
    if (fileUploadStatus === null) {
      try {
        const reader = new FileReader();
        reader.addEventListener(
          "load",
          async function () {
            // image file in base64 string
            try {
              const image = await jimp.read(reader.result);
              image.resize(200, 200);
              const imageData = await image.getBase64Async(jimp.MIME_JPEG);
              await setMemberProfile({
                memberUserId: selectedMember,
                profileImage: imageData,
                setUploadPercent,
              });
              setPage("uploadDone");
            } catch (error) {
              console.log(error);
              setPage("uploadFailed");
            }
          },
          false
        );
        reader.addEventListener("error", () => {
          console.error("error in uploadProfileImage fileReader");
          setPage("uploadFailed");
        });
        if (file) {
          reader.readAsDataURL(file);
        }
      } catch (error) {
        console.log(error);
        setPage("uploadFailed");
      }
    }

    if (fileUploadStatus !== "uploading") {
      setFileUploadStatus("uploading");
    }

    return <div>{fileUploadStatus}</div>;
  }
  return (
    <div className="Area uploadFileArea">
      <BackBtn
        onClick={() => {
          onClose();
        }}
      />
      {animation && (
        <SimpleLottieRunner
          animationData={animation}
          onComplete={() => {}}
          onlyRender={false}
          isPaused={false}
          isStopped={false}
          autoplay
          loop={loop}
          speed={1}
          width="100%"
          height="100%"
        />
      )}
      {activePage === "selectFile" && (
        <div className="upload-page">
          <input
            type="file"
            accept="image/*"
            className="fileInput"
            onChange={(event) => {
              console.log(event.target.files);
              setFile(event.target.files[0]);
            }}
          />
          <button
            className="startUpload"
            onClick={() => {
              setPage("uploading");
            }}
          ></button>
          <div className="additionalData">
            <div className="selectedMember">
              <select
                value={selectedMember}
                onChange={(e) => {
                  console.log(e.target.value);
                  setSelectedMember(e.target.value);
                }}
              >
                {familyMembers.map((member) => (
                  <option value={member.memberId}>
                    {member.firstname} {member.lastname}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      )}

      <div
        className="uploading"
        style={{
          visibility: activePage === "uploading" ? "visible" : "hidden",
        }}
      >
        <div className="uploadPercent">{uploadPercent}</div>
      </div>

      <div
        className="uploadDone"
        style={{
          visibility: activePage === "uploadDone" ? "visible" : "hidden",
        }}
      >
        <BackBtn
          onClick={() => {
            onClose();
          }}
        />
      </div>

      <div
        className="uploadFailed"
        style={{
          visibility: activePage === "uploadFailed" ? "visible" : "hidden",
        }}
      >
        <div
          className="retryBtn"
          onClick={() => {
            setPage("selectFile");
          }}
        ></div>
      </div>
    </div>
  );
};

export default UploadProfileImage;
