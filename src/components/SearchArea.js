import React, { useState } from "react";
import BackBtn from "./backBtn";
import SearchAreaSVG from "../resources/svg/search_window.svg";
import { searchUser } from "../api/index";
import store from "store";
import { setSelectedFamilyToVisit, setSelectedFamilyTitleToVisit } from "../tools/funcs";
// import bgBorder from "../resources/svg/bgBorder.svg";
// const searchData = [
//   {
//     name: "بنیامین بهادری"
//   }
// ];
const SearchArea = ({ onClose, onItemSelected }) => {
  const [searchData, setSearchData] = useState([]);
  const searchTxtRef = React.createRef();
  React.useEffect(() => {
    searchTxtRef.current.focus();
  });
  const [searchTxt, setSearchTxt] = React.useState("");
  return (
    <div className="searchArea">
      <img src={SearchAreaSVG} alt="searchArea" />
      <BackBtn onClick={() => onClose()} />
      <input
        ref={searchTxtRef}
        value={searchTxt}
        type="text"
        name="searchTxt"
        className="searchTxt"
        onChange={async e => {
          const val = e.currentTarget.value;
          console.log(val);
          setSearchTxt(val);
          if (val.trim().length === 0) return setSearchData([]);
          const res = await searchUser(val);
          setSearchData(res.searchResults);
        }}
      />

      <div className="searchResult">
        <ul>
          {searchData.length > 0 &&
            searchData.map(item => (
              <li
                onClick={e => {
                  // store.set("currentVisitingHome", item.familyId);
                  setSelectedFamilyToVisit(item.familyId);
                  setSelectedFamilyTitleToVisit(item.familyTitle);
                  onItemSelected(e);
                }}
              >
                {item.familyTitle}
                {!item.familyTitle &&
                  item.members &&
                  item.members[0] &&
                  `${item.members[0].firstname} ${item.members[0].lastname}`}
              </li>
            ))}
        </ul>
      </div>
    </div>
  );
};

export default SearchArea;
