import React, { createRef, useEffect, useState, useCallback } from "react";
import Lottie from "react-lottie";
export default function SimpleLottieRunner(props) {
  let {
    id = 0,
    isPaused = true,
    isStopped = true,
    autoplay = false,
    loop = false,
    speed = 1,
    width = 100,
    height = 100,
    from,
    to,
    direction = 1,
    onlyRender = false,
    animationData,
    callbacks,
    onComplete,
  } = props;
  let lottie = createRef();

  const callbacksToProcess = callbacks;
  console.log("in SimpleLottieRunner", id, props);
  useEffect(() => {
    console.log("SimpleLottieRunner useEffect", id, lottie, direction);
    if (autoplay) {
      const endFrame = to || lottie.current.anim.getDuration(true);
      if (onlyRender) {
        console.log("goToAndStop", { id, endFrame });
        lottie.current.anim.goToAndStop(endFrame, true);
      } else {
        lottie.current.anim.setSpeed(speed);
        lottie.current.anim.goToAndStop(from, true);
        if (direction === 1) {
          console.log("goToAndStop", id, from || 0);
          lottie.current.anim.goToAndStop(from || 0, true);
        } else {
          console.log(
            "goToAndStop",
            id,
            from || lottie.current.anim.getDuration(true)
          );
          lottie.current.anim.goToAndStop(
            from || lottie.current.anim.getDuration(true),
            true
          );
        }
        lottie.current.anim.setDirection(direction);
        lottie.current.anim.play();
      }
    }
  }, [autoplay, isPaused, isStopped]);
  const defaultOptions = {
    loop,
    autoplay,
    animationData: animationData && animationData.default,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <div>
      <Lottie
        ref={lottie}
        isClickToPauseDisabled={true}
        direction={direction}
        options={defaultOptions}
        speed={speed}
        height={height}
        width={width}
        isStopped={isStopped}
        isPaused={isPaused}
        eventListeners={[
          {
            eventName: "enterFrame",
            callback: (data) => {
              // console.log("in enter frame", {
              //   lottie,
              //   id,
              //   direction,
              //   to,
              //   currentTime: data.currentTime,
              // });
              if (
                (direction === 1 && to && data.currentTime >= to) ||
                (direction === -1 && to && data.currentTime <= to)
              ) {
                console.log("stop animation", direction, to, lottie, data);
                if (lottie.current) lottie.current.anim.pause();
              }

              if (callbacksToProcess) {
                // console.log("callbacks", callbacksToProcess);
                callbacks.forEach((c, index) => {
                  // console.log(c, index, data.currentTime);
                  if (
                    !c.processed &&
                    c.event === "onEnterFrame" &&
                    Math.abs(c.frame - data.currentTime) < 1
                  ) {
                    console.log("matched", c);
                    c.run();
                    callbacks[index].processed = true;
                  }
                });
              }
            },
          },
          {
            eventName: "complete",
            callback: (data) => {
              console.log("onComplete", callbacks);
              onComplete && onComplete();

              callbacks &&
                callbacks
                  .filter((c) => c.event === "onComplete")
                  .forEach((callback) => {
                    callback.run();
                  });
            },
          },
        ]}
      />
    </div>
  );
}
