import React from "react";
import acceptSVG from "../resources/svg/accept-btn.svg";

const AcceptBtn = ({ onClick, width = "100%", height = "100%" }) => {
  return (
    <img
      src={acceptSVG}
      className="AcceptBtn"
      width={width}
      height={height}
      alt="AcceptBtn"
      onClick={onClick}
    />
  );
};

export default AcceptBtn;
