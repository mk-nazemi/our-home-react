export function reverseIt({ transition, speed = 4, onComplete }) {
  const t = { ...transition };

  t.direction = -1;
  t.speed = 4;
  t.callbacks = [
    {
      event: "onComplete",
      run: onComplete
    }
  ];
  return t;
}
