import store from "store";
import millify from "millify";
import { logError } from "../api";
import { PERMISSION_LEVELS } from "../constants";
import { USER_PROFILE_PIC_URL_TEMPLATE } from "../config";
export function parseArabic(str) {
  if (!str) return;
  return str
    .replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
      return d.charCodeAt(0) - 1632; // Convert Arabic numbers
    })
    .replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function (d) {
      return d.charCodeAt(0) - 1776; // Convert Persian numbers
    });
}

export function prettyNum(num) {
  if (isNaN(num)) return;
  return millify(num, {
    precision: 2,
    decimalSeparator: ",",
  });
}
export function correctPhoneNumber(num) {
  if (!num) return "";
  let number = num;
  number = number.split(" ").join();
  number = parseArabic(number);
  if (num.length === 11) {
    number = `98${num.substring(1)}`;
  } else if (num.length === 10) number = `98${number}`;
  return number;
}
export function downloadURI(uri, name) {
  var link = document.createElement("a");
  link.download = name;
  link.target = "_blank";
  link.href = uri;
  link.click();
}

export function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return "";
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export function getFamilyTags() {
  return Object.keys(store.get("familyTags") || []);
}
export function getMyHomes() {
  return Object.keys(store.get("familyTags") || []);
}
export function getMyFirstFamilyId() {
  return getMyHomes()[0];
}
export function isItMyHome() {
  // console.log(getSelectedFamilyToVisit(), getMyFirstFamilyId(), getMyHomes());
  return (
    getSelectedFamilyToVisit() === undefined ||
    getMyHomes().some((myFamilyId) => myFamilyId === getSelectedFamilyToVisit())
  );
}

export function saveMyStatus(data) {
  store.set("pendingInvitations", data.pendingInvitations);
  store.set("accessTags", data.accessTags);
  store.set("familyTags", data.familyTags);
}

export function getPendingInvitations() {
  return store.get("pendingInvitations") || [];
}

export function clearPersistStates() {
  // saving otpToken
  const otpToken = store.get("otpToken");
  store.clearAll();
  store.set("otpToken", otpToken);
}
export function gotoMyHome() {
  store.set("currentVisitingHome", getMyFirstFamilyId());
}

export function getSelectedFamilyToVisit() {
  return store.get("currentVisitingHome") || getMyFirstFamilyId();
}

export function setSelectedFamilyToVisitInfo(info) {
  return store.set("selectedFamilyToVisitInfo", info);
}

export function getSelectedFamilyToVisitInfo(
  familyId = getSelectedFamilyTitleToVisit()
) {
  return store.get("selectedFamilyToVisitInfo") || {};
}

export function setSelectedFamilyToVisit(familyId) {
  console.log("setSelectedFamilyToVisit", familyId);
  store.set("currentVisitingHome", familyId);
}

export function setSelectedFamilyTitleToVisit(familyTitle) {
  store.set("selectedFamilyTitleToVisit", familyTitle);
}

export function getSelectedFamilyTitleToVisit(familyTitle) {
  return store.get("selectedFamilyTitleToVisit", familyTitle);
}

export function isCreatedFamily() {
  return store.get("isCreatedFamily");
}

export function getLastOpenedPage() {
  return store.get("lastOpenedPage");
}

// const ViewPermissions = [{

// }]

// check member level permission on page
export function checkFamilyPermission(
  permissions,
  page,
  level = PERMISSION_LEVELS.VIEW,
  memberId
) {
  console.log(permissions, page, level, memberId);
  if (!permissions || !permissions[page] || !permissions[page][level])
    return false;
  return permissions[page][level].some((member) => member._id === memberId);
}
export function hasAdminAccessTo(familyId, section = "*") {
  console.log(familyId, section);
  return hasPermission(familyId, section, PERMISSION_LEVELS.ADMIN);
}

export function hasPermission(familyId, section, permission) {
  const accessTags = store.get("accessTags");
  if (!accessTags) return false;
  if (!familyId) return true;

  console.log(accessTags, familyId);
  return Object.entries(accessTags).some(
    (entry) =>
      entry[0] === familyId &&
      (entry[1][section] === permission ||
        entry[1]["*"] === permission ||
        entry[1]["*"] === PERMISSION_LEVELS.ADMIN)
  );
}
export function hasPermissionTo(
  section = getLastOpenedPage(),
  familyId = getSelectedFamilyToVisit()
) {
  const accessTags = store.get("accessTags");
  if (!accessTags) return false;
  if (!familyId) return true;
  // if(section === '*') return true;
  // for now just check whole access, assume that if key exists user has access
  // if(accessTags[familyId] === undefined) return false;

  console.log(accessTags, familyId);
  return Object.entries(accessTags).some(
    (entry) =>
      entry[0] === familyId &&
      (entry[1][section] === PERMISSION_LEVELS.VIEW ||
        entry[1]["*"] === PERMISSION_LEVELS.VIEW ||
        entry[1][section] === PERMISSION_LEVELS.ADMIN ||
        entry[1]["*"] === PERMISSION_LEVELS.ADMIN)
  );
}

export function isCreator(familyId) {}
export function enableErrorLog() {
  window.onerror = console.error = (...args) => {
    try {
      // console.log("log error", args);
      console.log("Error", args);
      // const phoneNumber = store.get("otp-number");
      // logError({ ...args, phoneNumber });
    } catch (error) {
      console.log("error in logging error");
    }
  };
}

export function toUpperFirstLetter(str) {
  if (!str || !str.length) return "";
  return `${str.charAt(0).toUpperCase()}${str.slice(1)}`;
}

export function generateMemberImage(familyId, memberId) {
  return USER_PROFILE_PIC_URL_TEMPLATE.replace("familyId", familyId)
    .replace("memberId", memberId)
    .replace("random", Math.random());
}

export function getImgThumb(file) {
  return `https://fs.incredibleswebapplication.ir/thumbnails/${file.externalId}-thumbnail.jpg`;
}

export function getVideoThumb(file) {
  return `https://fs.incredibleswebapplication.ir/video-thumbs/${file.externalId}.jpg`;
}
