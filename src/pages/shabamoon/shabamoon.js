import React, { useState, useEffect } from "react";
import BackBtn from "../../components/backBtn";
import VideoPlayer from "../../components/VideoPlayer";
import store from "store";
import {
  userSections,
  transitions,
  lalayeeamoonVideos,
  pages,
} from "../../constants";
import AccessArea from "../../components/AccessArea";
import { getFilesAPI } from "../../api";
import { getSelectedFamilyToVisit } from "../../tools/funcs";
import { reverseIt } from "../../tools/lottieHelpers";

const Shabamoon = ({ state, setState, runTransition, gotoPage, page }) => {
  const [files, setFiles] = useState([]);
  async function getFiles() {
    const familyId = getSelectedFamilyToVisit();
    const files = await getFilesAPI(familyId, page.name);
    console.log(files);
    setFiles(files);
  }

  useEffect(() => {
    console.log("run baziamoon transition");
    const t = transitions.shabamoon;
    runTransition(t);
  }, []);

  useEffect(() => {
    getFiles();
  }, []);

  return (
    <div className="shabamoon-page">
      <BackBtn
        onClick={() => {
          const reverse = reverseIt({
            transition: transitions.shabamoon,
            onComplete: () => gotoPage({ page: pages.home }),
          });
          runTransition(reverse);
        }}
      />

      <AccessArea
        upload={{
          fileType: page.uploadFileType,
          contentType: page.uploadFileType,
          familyId: Object.keys(store.get("familyTags"))[0],
          place: page.name,
          validTypes: page.validTypes,
          // type: "public",
          metadata: {},
          onClosed: () => {
            console.log("closed");
            getFiles();
          },
        }}
        page={page}
      />
      <div className="playerArea">
        <VideoPlayer videos={files} />
      </div>
      <div
        className="gotoChallengeBtn"
        onClick={() => {
          // runPagesTransition({ ...transitions["shabamoon-to-challenjamoon"] });
          // runTransition({ ...transitions["shabamoon-to-challenjamoon"] });
        }}
      ></div>
    </div>
  );
};

export default Shabamoon;
