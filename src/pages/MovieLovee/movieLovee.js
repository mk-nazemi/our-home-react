import React, { useState, useEffect } from "react";
import store from "store";
import BackBtn from "../../components/backBtn";
import { transitions, pages } from "../../constants";
import VideoPlayer from "../../components/VideoPlayer";
import AccessArea from "../../components/AccessArea";
import { getFilesAPI } from "../../api";
import { getSelectedFamilyToVisit } from "../../tools/funcs";
import { reverseIt } from "../../tools/lottieHelpers";
const MovieLovee = ({ state, setState, runTransition, gotoPage, page }) => {
  const [files, setFiles] = useState([]);
  async function getFiles() {
    const familyId = getSelectedFamilyToVisit();
    const files = await getFilesAPI(familyId, page.name);
    console.log(files);
    setFiles(files);
  }

  useEffect(() => {
    console.log("run baziamoon transition");
    const t = transitions.movieLovee;
    runTransition(t);
  }, []);

  useEffect(() => {
    getFiles();
  }, []);
  return (
    <div className="movieLovee-page">
      <BackBtn
        onClick={() => {
          const reverse = reverseIt({
            transition: transitions.movieLovee,
            onComplete: () => gotoPage({ page: pages.filmamoon }),
          });
          runTransition(reverse);
        }}
      />
      <AccessArea
        upload={{
          fileType: page.uploadFileType,
          contentType: page.uploadFileType,
          familyId: Object.keys(store.get("familyTags"))[0],
          place: page.name,
          validTypes: page.validTypes,
          // type: "public",
          metadata: {},
          onClosed: () => {
            console.log("closed");
            getFiles();
          },
        }}
        page={page}
      />
      <div className="playerArea">
        <VideoPlayer videos={files} />
      </div>
    </div>
  );
};

export default MovieLovee;
