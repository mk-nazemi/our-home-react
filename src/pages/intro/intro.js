import React, { useEffect, useState } from "react";
// import SearchArea from "../../components/SearchArea";
import SearchPage from "../../pages/search/search";
import store from "store";
import classNames from "classnames";
import { transitions, pages } from "../../constants";
import { searchUser } from "../../api";
import explorerFireSVG from "../../resources/svg/8 box ba atish.svg";
import explorerSVG from "../../resources/svg/8 box.svg";
import {
  setSelectedFamilyToVisit,
  getFamilyTags,
  getPendingInvitations,
  generateMemberImage,
  setSelectedFamilyTitleToVisit,
} from "../../tools/funcs";
import SimpleLottieRunner from "../../components/SimpleLottieRunner";

// const images = [
//   {
//     id: "5da64b8be8b5dcc8ca6600e8",
//     link: "https://assets-khunamoon.serveapp.ir/axamoon/10.jpg"
//   }
// ];

// console.log("images", images);
const explorerData = [
  {
    familyId: "P4EFVh3YVM",
    familyScore: 58.68454430504324,
    familyTitle: "Rahimi",
    members: [
      {
        memberId: "5e80d341d4c9cf0008f3e82a",
        role: "فرزند پسر",
        firstname: "Omid",
        lastname: "Rahimi",
      },
    ],
  },
  {
    familyId: "r36vlnmguo",
    familyScore: 33.89222333009806,
    familyTitle: "میرخانی",
    members: [
      {
        memberId: "5e8c3ca4ec43850008e990cb",
        role: "تنهام",
        firstname: "ماندانا",
        lastname: "میرخانی",
      },
    ],
  },
  {
    familyId: "e3tm22hwik",
    familyScore: 18.295651830906092,
    familyTitle: "خونمون",
    members: [
      {
        memberId: "5e8b362f9e3df50009fd875d",
        role: "فرزند پسر",
        firstname: "امید",
        lastname: "خونمون",
      },
    ],
  },
  {
    familyId: "oq0hg2sr7y",
    familyScore: 15.120221783451091,
    familyTitle: "xyz",
    members: [
      {
        memberId: "5e8b9e0c9e3df50009fd87bb",
        role: "تنهام",
        firstname: "abc",
        lastname: "xyz",
      },
    ],
  },
  {
    familyId: "B3dsV5Bc39",
    familyScore: 14.675919548437115,
    familyTitle: "mahmoudi",
    members: [],
  },
  {
    familyId: "wcasu77cfb",
    familyScore: 13.952104429252955,
    familyTitle: "Raeisi",
    members: [
      {
        memberId: "5e898bedb2b4380008c71bf6",
        role: "پدر",
        firstname: "Milad",
        lastname: "Raeisi",
      },
      {
        memberId: "5e898b87b2b4380008c71bf5",
        role: "فرزند پسر",
        firstname: "Milad2",
        lastname: "Raeisi",
      },
    ],
  },
  {
    familyId: "B3dsV5Bc39",
    familyScore: 14.675919548437115,
    familyTitle: "mahmoudi",
    members: [],
  },
  {
    familyId: "B3dsV5Bc39",
    familyScore: 14.675919548437115,
    familyTitle: "mahmoudi",
    members: [],
  },
];
const EXPLORER_LIMIT = 8;

const Intro = ({ setState, runTransition, gotoPage, onlyRender }) => {
  const [loadedExplorer, setLoadedExplorer] = useState([]);
  const [loadedExplorerStart, setLoadedExplorerStart] = useState(0);

  const [explorerAnimationVisible, setExplorerAnimationVisible] = useState(
    true
  );
  useEffect(() => {
    const t = transitions.intro;
    runTransition(t);
  }, []);

  useEffect(() => {
    async function loadExplorer() {
      const searchRes = await searchUser("", 0, 64);
      console.log(searchRes);
      setLoadedExplorer(searchRes.searchResults);
      // const d = [].concat(
      //   ...Array.from({ length: EXPLORER_LIMIT }, () => explorerData)
      // );
      // console.log(d);
      // setLoadedExplorer(d);
    }
    loadExplorer();
  }, []);
  const numberTxtRef = React.createRef();
  const [showSearchArea, setShowSearchArea] = React.useState(false);
  return (
    <div className="intro-page">
      <div className={classNames("introPageContent", { hide: showSearchArea })}>
        <div
          className="khunamoooBebinBtn"
          onClick={() => {
            gotoPage({
              page: pages.search,
              backTo: pages.intro,
              backWithoutAnimation: true,
            });
          }}
        ></div>
        <div
          onClick={async () => {
            console.log(getFamilyTags());
            if (getFamilyTags().length) {
              setSelectedFamilyToVisit(getFamilyTags()[0]);
              gotoPage({ page: pages.doorBell });
              return;
            }
            const requests = getPendingInvitations();
            console.log(requests);
            if (requests.length > 0) {
              return gotoPage({
                page: pages.invitationRequests,
                backTo: pages.intro,
                backWithoutAnimation: true,
              });
            }
            gotoPage({
              page: pages.khunatoBesaz,
              backTo: pages.intro,
              backWithoutAnimation: true,
            });
          }}
          className="khunatoonoBesazBtn"
        ></div>
        <div
          className="hottestArea"
          onTouchMove={(e) => {
            // console.log(e);
          }}
        >
          {/* <div
            onClick={() => {
              setTimeout(() => {
                if (loadedExplorerStart - EXPLORER_LIMIT >= 0)
                  setLoadedExplorerStart(loadedExplorerStart - EXPLORER_LIMIT);
              }, 1000);
            }}
          >
            prev
          </div>
          <div
            onClick={() => {
              const newStart = loadedExplorerStart + EXPLORER_LIMIT;
              setLoadedExplorerStart(newStart);
              console.log(
                loadedExplorerStart,
                EXPLORER_LIMIT,
                loadedExplorer.slice(newStart)
              );
              if (loadedExplorer.slice(newStart).length === 0) {
                setTimeout(() => {
                  const newData = [...explorerData];
                  const cp = [...loadedExplorer];
                  for (let index = 0; index < newData.length; index++) {
                    cp[newStart + index] = newData[index];
                  }
                  console.log("new explorer", cp);
                  setLoadedExplorer(cp);
                }, 1000);
              }
            }}
          >
            next
          </div> */}
          <div>
            <div
              style={{ display: explorerAnimationVisible ? "block" : "none" }}
            >
              <SimpleLottieRunner
                animationData={transitions.explorer.animationData}
                onComplete={() => {
                  console.log("completed");
                  setExplorerAnimationVisible(false);
                }}
                onlyRender={false}
                isPaused={false}
                isStopped={false}
                autoplay
                loop={false}
                speed={1}
                width="100%"
                height="100%"
              />
            </div>
            {loadedExplorer &&
            loadedExplorer.length >= loadedExplorerStart + EXPLORER_LIMIT
              ? loadedExplorer
                  .reduce((acc, item, index) => {
                    // console.log(acc[index % EXPLORER_LIMIT], acc, index);
                    acc[index % EXPLORER_LIMIT]
                      ? acc[index % EXPLORER_LIMIT].push(item)
                      : (acc[index % EXPLORER_LIMIT] = [item]);
                    return acc;
                  }, [])
                  // .slice(
                  //   loadedExplorerStart,
                  //   loadedExplorerStart + EXPLORER_LIMIT
                  // )
                  // .filter(image => image)
                  .map((bucket, index) => {
                    // console.log("item", bucket, index);
                    return (
                      <div
                        className="explorerBucket"
                        style={
                          {
                            // backgroundImage: `url(${explorerSVG})`,
                            // backgroundSize: "cover",
                          }
                        }
                      >
                        {index === 0 ? (
                          <img src={explorerFireSVG} alt="bg" />
                        ) : (
                          <img src={explorerSVG} alt="bg" />
                        )}
                        <div className={`bucketItems bucket-${index}`}>
                          {bucket.map((item, index) => (
                            <div
                              className={`item box${index + 1}`}
                              onClick={() => {
                                setSelectedFamilyToVisit(item.familyId);
                                setSelectedFamilyTitleToVisit(item.familyTitle);
                                gotoPage({ page: pages.doorBell });
                              }}
                            >
                              <div className="itemName">{item.familyTitle}</div>
                              <div key={index} className={`imgArea`}>
                                {item.members && item.members[0] && (
                                  <img
                                    src={generateMemberImage(
                                      item.familyId,
                                      item.members[0].memberId
                                    )}
                                    onError={(e) => {
                                      // console.log("image not found error", e);
                                      e.target.style.display = "none";
                                    }}
                                    alt="userProfileImage"
                                  />
                                )}
                              </div>
                            </div>
                          ))}
                        </div>
                      </div>
                    );
                  })
              : [...Array(EXPLORER_LIMIT).keys()].map((item, index) => (
                  <div className={`item box${index + 1}`}>
                    <div className="itemName">Loading</div>
                  </div>
                ))}
          </div>
        </div>
        )
      </div>
      <div className="searchAreaPopup">
        {/* {showSearchArea && (
          <SearchArea
            showSearchArea={showSearchArea}
            onClose={() => setShowSearchArea(false)}
            onItemSelected={item => {
              setShowSearchArea(false);
              runTransition(transitions["intro-search-to-doorbell"]);
            }}
          />
        )} */}
      </div>
    </div>
  );
};

export default Intro;
