import React, { useEffect, useState, createRef, useRef } from "react";
// import io from "socket.io-client";
import RTCMultiConnection, { getHTMLMediaElement } from "rtcmulticonnection";
import jwtDecode from "jwt-decode";
import store from "store";
import adapter from "webrtc-adapter";
import SimpleLottieRunner from "../../components/SimpleLottieRunner";
import onlineIcon from "../../resources/svg/online.svg";
import callRequestIcon from "../../resources/svg/callRequest.svg";
import acceptCallIcon from "../../resources/svg/acceptCall.svg";
import rejectCallIcon from "../../resources/svg/rejectCall.svg";
import endCallIcon from "../../resources/svg/endCall.svg";
// import * as onlineLottieData from "../../resources/lottieFiles/online.json";
import * as callingLottieData from "../../resources/lottieFiles/calling.json";
import BackBtn from "../../components/backBtn";
import { transitions, images, pages } from "../../constants";
import BG from "../../resources/svg/border2.svg";
import { authenticate } from "../../api";
import { getSelectedFamilyToVisit } from "../../tools/funcs";
// var classNames = require("classnames");
let connection, socket;

const Didemoon = ({ state, setState, runTransition, gotoPage, type }) => {
  // const myFamilyId = "123";
  // const caleeFamilyId = store.get("currentVisitingHome");
  // const caleeFamilyId = "123";
  const [onlineFamilyMembers, setOnlineFamilyMembers] = useState([]);
  const [callRequests, setCallRequests] = useState([]);
  const [videos, setVideos] = useState({});
  const [callStatus, setCallStatus] = useState("init");
  const [mainVideoIndex, setMainVideoIndex] = useState(0);

  console.log("rendering dedemoon", mainVideoIndex);

  //   const [videos, setVideos] = useState({});
  const [displayAnswerCall, setDisplayAnswerCall] = useState(false);
  const [onlineTransition, setOnlineTransition] = useState(null);
  // let PR = createRef();
  // const [videoRefs, setVideoRefs] = useState([]);
  // const videoRefs = [];
  function initialize() {
    console.log("initilazing");
    connection = new RTCMultiConnection();
    // connection.socketURL = "http://localhost:3001/";
    connection.socketURL = "https://liveapi.mabeyn.ir/";
    connection.maxParticipantsAllowed = 4;
    connection.session = {
      audio: true,
      video: true,
    };

    connection.iceServers = [];
    // second step, set STUN url
    connection.iceServers.push({
      urls: "stun:stun.l.google.com:19302",
    });

    // last step, set TURN url (recommended)
    connection.iceServers.push({
      urls: "turn:185.236.37.109:3478",
      credential: "test",
      username: "test",
    });

    // set this line to close room as soon as owner leaves
    connection.autoCloseEntireSession = true;

    connection.sdpConstraints.mandatory = {
      OfferToReceiveAudio: true,
      OfferToReceiveVideo: true,
    };

    // connect(Math.random());
    // const decoded = jwtDecode(store.get("userToken"));
    // console.log(decoded);
    // connect(decoded.uid);
    connect();
  }
  async function connect(userId) {
    await authenticate();
    console.log("connecting");
    // const userId = document.getElementById("userId").value;
    // localStorage.setItem("userId", userId);
    const userToken = store.get("userToken");
    // console.log(userToken);
    // const familyTag = Object.keys(store.get("familyTags"))[0];
    // const myFamilyId = familyTag;
    // const currentVisitingHome = getSelectedFamilyToVisit();
    // const calleeFamilyId = type === "didemoon" ? currentVisitingHome : null;
    // connect socket and get it
    connection.socketCustomParameters = `&userToken=${userToken}`;
    // connection.socketCustomParameters = `&userId=${userId}`;
    socket = connection.getSocket();

    socket.on("onlineFamilyMembers", (data) => {
      console.log("onlineFamilyMembers", data);
      setOnlineFamilyMembers(data);
    });
    socket.on("imOnline", (data) => {
      console.log("imOnline", data);
      const users = [...onlineFamilyMembers];
      users.push(data.userId);
      setOnlineFamilyMembers(users);
    });

    socket.on("imOffline", (data) => {
      console.log("imOffline", data);
      let users = [...onlineFamilyMembers];
      users = users.filter((user) => user !== data.userId);
      setOnlineFamilyMembers(users);
    });

    socket.on("callRequestRes", (res) => {
      if (res.error) {
        setCallStatus("idle");
        return console.error(res.error);
      }
      console.log(`${res.status} ${res.state}`);
      if (res.status === "ALREADY_IN_SESSION") {
        connection.join(res.sessionId, function (isJoined, roomid, error) {
          if (!isJoined) return console.error(error);
          console.log("joined", res.sessionId);
          setCallStatus("accepted");
        });
      }
    });

    socket.on("callRequest", (data) => {
      console.log("callRequest", data);
      const callRequestsCopy = [...callRequests];
      callRequestsCopy.push(data);
      console.log(callRequestsCopy);
      setCallRequests(callRequestsCopy);
    });

    socket.on("callRequestAccepted", (data) => {
      console.log("join room", data, data.sessionId);
      connection.join(data.sessionId, function () {
        console.log("joined", data.sessionId);
      });
    });

    socket.on("callRequestAcceptedRes", (data) => {
      console.log("callRequestAcceptedRes", data);
      if (data.status === "OK") return setCallStatus("inCall");
    });

    socket.on("callRequestRejectedRes", (data) => {
      console.log("callRequestRejectedRes", data);
      setCallStatus("rejected");
    });

    socket.on("callRequestRejected", (data) => {
      setCallStatus("rejected");
    });

    socket.on("onlineRes", (data) => {
      connection.open(data.sessionId, function (data) {
        console.log("opened room", data);
      });
    });

    socket.on("sessionFinished", () => {
      console.log("sessionFinished");
      setCallStatus("init");
      closeSession();
    });
    socket.on("serverIsReady", () => {
      openNewSession();
    });

    socket.on("error", console.error);
  }

  function openNewSession() {
    // say im online for call
    if (type === "bazdidemoon") socket.emit("CalleeIsReady");
  }
  function closeSession() {
    connection.getAllParticipants().forEach(function (participantId) {
      connection.disconnectWith(participantId);
    });
    connection.closeEntireSession();
    connection.dontCaptureUserMedia = true;
  }
  function leave() {
    connection.closeSocket();
  }
  function call(userId) {
    setCallStatus("calling");
    setCallStatus("inCall");
    socket.emit("callRequest", { hostId: userId });
  }
  function acceptCall(index) {
    if (!callRequests[index]) return;
    const { sessionId, guestId } = callRequests[index];
    socket.emit("callRequestAccepted", { sessionId, guestId });

    console.log(index, callRequests);
    const cp = [...callRequests];
    cp.splice(index, 1);
    setCallRequests(cp);
  }
  function rejectCall(index) {
    if (!callRequests[index]) return;
    const { sessionId, guestId } = callRequests[index];
    socket.emit("callRequestRejected", { sessionId, guestId });

    const cp = [...callRequests];
    cp.splice(index, 1);
    setCallRequests(cp);
  }
  function disconnectWith(userId) {
    connection.disconnectWith(userId);
  }
  useEffect(() => {
    initialize();

    return () => {
      console.log("cleanup");

      // if (connection) {
      //   connection.closeEntireSession();
      //   connection.closeSocket();
      // }
    };
  }, []);

  const [
    connectionListenersInitiliazed,
    setConnectionListenersInitiliazed,
  ] = useState(false);
  useEffect(() => {
    // if (connectionListenersInitiliazed) return;
    // setConnectionListenersInitiliazed(true);
    connection.onstream = function (event) {
      console.log("onstream", event, videos);
      if (event.mediaElement) {
        // event.mediaElement.muted = true;
        // delete event.mediaElement;
        // event.mediaElement.removeAttribute("src");
        // event.mediaElement.removeAttribute("srcObject");
        // event.mediaElement.muted = true;
        // event.mediaElement.volume = 0;
      }
      const cp = { ...videos };
      const v = { event };
      if (Object.entries(cp).length === 0) v.main = true;
      else v.main = false;
      if (event.type === "local") {
        v.muted = true;
      }
      cp[event.streamid] = v;
      setVideos(cp);
    };
    // connection.videosContainer = document.getElementById('videos-container');
    // connection.onstream = function(event) {
    //   var existing = document.getElementById(event.streamid);
    //   if (existing && existing.parentNode) {
    //     existing.parentNode.removeChild(existing);
    //   }

    //   event.mediaElement.removeAttribute("src");
    //   event.mediaElement.removeAttribute("srcObject");
    //   event.mediaElement.muted = true;
    //   event.mediaElement.volume = 0;

    //   var video = document.createElement("video");

    //   try {
    //     video.setAttributeNode(document.createAttribute("autoplay"));
    //     video.setAttributeNode(document.createAttribute("playsinline"));
    //   } catch (e) {
    //     video.setAttribute("autoplay", true);
    //     video.setAttribute("playsinline", true);
    //   }

    //   if (event.type === "local") {
    //     video.volume = 0;
    //     try {
    //       video.setAttributeNode(document.createAttribute("muted"));
    //     } catch (e) {
    //       video.setAttribute("muted", true);
    //     }
    //   }
    //   video.srcObject = event.stream;

    //   var width = parseInt(connection.videosContainer.clientWidth / 3) - 20;
    //   var mediaElement = getHTMLMediaElement(video, {
    //     title: event.userid,
    //     buttons: ["full-screen"],
    //     width: width,
    //     showOnMouseEnter: false
    //   });

    //   connection.videosContainer.appendChild(mediaElement);

    //   setTimeout(function() {
    //     mediaElement.media.play();
    //   }, 5000);

    //   mediaElement.id = event.streamid;
    // };
    connection.onstreamended = function (event) {
      console.log("onstreamended", event);
      const cp = { ...videos };
      delete cp[event.streamid];
      setVideos(cp);
    };
    connection.onEntireSessionClosed = function (event) {
      console.info("Entire session is closed: ", event.sessionid, event.extra);
      const justLocal = Object.entries(videos)
        .filter((entry) => entry[1].event.type === "local")
        .reduce((obj, entry) => {
          obj[entry[0]] = entry[1];
          return obj;
        }, {});
      console.log(videos, justLocal);
      setVideos(justLocal);
    };
  }, [connectionListenersInitiliazed, videos]);

  return (
    <div className="didemoon-page">
      <BackBtn
        onClick={() => {
          Object.values(videos).map((video) => {
            console.log(video);
            const stream = video.event.stream;
            stream.getTracks().forEach(function (track) {
              track.stop();
            });
          });
          if (connection) {
            connection.closeEntireSession();
            connection.closeSocket();
          }
          gotoPage({ page: pages.home });
        }}
      />

      <ul className="requestForCall">
        {type === "didemoon" &&
          callStatus !== "inCall" &&
          onlineFamilyMembers.map((user) => {
            const name = user.slice(user.indexOf("-") + 1);
            return (
              <li
                onClick={() => {
                  call(user);
                }}
              >
                <span>{name}</span>
                <img src={onlineIcon} alt="online" className="onlineIcon" />
                <img
                  src={callRequestIcon}
                  alt="callRequest"
                  className="callRequestIcon"
                />
              </li>
            );
          })}
      </ul>

      {type === "bazdidemoon" &&
        connection &&
        connection.getAllParticipants().length > 0 && (
          <img
            src={endCallIcon}
            alt="endSessionIcon"
            className="endSessionIcon"
            onClick={() => {
              console.log("closeEntireSession");
              closeSession();
              socket.emit("sessionFinished");
            }}
          />
        )}
      <img src={BG} alt="bg" />

      <div className="callRequests">
        {callRequests.map((callReq, index) => (
          <div>
            <span>from {callReq.guestId}</span>
            <img
              className="acceptCall"
              src={acceptCallIcon}
              onClick={() => {
                acceptCall(index);
              }}
              alt="acceptCall"
            />
            <img
              className="rejectCall"
              src={rejectCallIcon}
              onClick={() => {
                rejectCall(index);
              }}
              alt="rejectCall"
            />
          </div>
        ))}
      </div>
      {callStatus === "calling" && (
        <SimpleLottieRunner
          animationData={callingLottieData}
          autoplay
          loop
          speed={4}
          width={"50%"}
          height={"50%"}
        />
      )}

      <div id="videos-container">
        <div className="main-video">
          {Object.values(videos)
            .filter((video, index) => index === mainVideoIndex)
            .map((video) => (
              <video
                // muted={video.muted}
                autoPlay
                playsInline
                ref={(ref) => {
                  if (!ref) return;
                  ref.srcObject = video.event.stream;
                  ref.className = "main";
                  if (video.muted) {
                    ref.muted = true;
                    ref.volume = 0;
                  }
                  setTimeout(() => {
                    ref.play();
                  }, 5000);
                }}
              />
            ))}
        </div>
        {Object.entries(videos)
          // .filter((video, index) => index !== mainVideoIndex)
          .map((entry, index) => {
            if (index === mainVideoIndex) return "";
            const video = entry[1];
            return (
              <div className="videoContainer">
                <video
                  muted={video.muted}
                  onClick={() => {
                    console.log("setMainViewIndex to", index);
                    setMainVideoIndex(index);
                  }}
                  autoPlay
                  playsInline
                  ref={(ref) => {
                    if (!ref) return;
                    ref.srcObject = video.event.stream;
                    if (video.muted) {
                      ref.muted = true;
                      ref.volume = 0;
                    }
                    setTimeout(() => {
                      ref.play();
                    }, 5000);
                  }}
                />
                {video.event.type !== "local" ? (
                  <img
                    src={endCallIcon}
                    alt="endCallIcon"
                    className="endCallIcon"
                    onClick={() => {
                      disconnectWith(video.event.userid);
                    }}
                  />
                ) : null}
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default Didemoon;
