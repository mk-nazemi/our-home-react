import React, { useState, createRef } from "react";
import SearchArea from "../../components/SearchArea";
import { pages, transitions, strings, RELATIONS } from "../../constants";
// import SearchAreaSVG from "../../resources/svg/search_window.svg";
import bgBorder from "../../resources/svg/big-border.svg";
import BackBtn from "../../components/backBtn";
import userInfoSVG from "../../resources/svg/userInfo.svg";
import addMemberSVG from "../../resources/svg/add-member.svg";
import plusSVG from "../../resources/svg/plus.svg";
import acceptSVG from "../../resources/svg/accept.svg";
import createHomeOkSVG from "../../resources/svg/create-home-ok.svg";
import createHomeFailedSVG from "../../resources/svg/create-home-failed.svg";
import deleteSVG from "../../resources/svg/deny.svg";
import editSVG from "../../resources/svg/edit.svg";
import { createFamily, addMember, getStatus } from "../../api";
import {
  // isCreatedFamily,
  getFamilyTags,
  getMyFirstFamilyId,
  correctPhoneNumber,
} from "../../tools/funcs";
import Popup from "../../components/Popup";
import store from "store";
import { getParameterByName } from "../../tools/funcs";
import OTPTextBox from "../../components/optTextBox";
const KhunatoBesaz = ({ back, onItemSelected, setState, runTransition }) => {
  const [showPopup, setShowPopup] = useState(false);
  const [popupContent, setPopupContent] = useState("");
  const [popupCloseCB, setPopupCloseCB] = useState({ callback: () => {} });

  const [activePage, setActivePage] = useState("main");
  const [userInfo, setUserInfo] = useState({
    firstName: "",
    lastName: "",
    relation: "",
  });
  const [activeMember, setActiveMember] = useState(0);
  const memberInitialVal = {
    firstName: "",
    relation: "",
    lastName: "",
    code: "",
    phoneNumber: "",
  };
  const [member, setMember] = useState(memberInitialVal);
  const [members, setMembers] = useState([]);

  const firstNameRef = createRef(null);
  const relationTxtRef = createRef(null);
  const lastNameRef = createRef(null);
  const phoneNumberRef = createRef(null);

  function removeMember(index) {
    let cp = [...members];
    console.log(cp, index);
    cp.splice(index, 1);
    setMembers(cp);
    setActiveMember(activeMember - 1);
    console.log(cp);
  }
  switch (activePage) {
    case "main":
      return (
        <div className="khunatoBesaz-page main">
          {showPopup && (
            <Popup
              content={popupContent}
              onClose={() => {
                setShowPopup(false);
                if (typeof popupCloseCB.callback === "function") {
                  popupCloseCB.callback();
                }
              }}
            />
          )}
          <img src={bgBorder} alt="searchArea" />
          <BackBtn
            onClick={() => {
              console.log(members);
              const isCreatedFamily = store.get("isCreatedFamily") || false;
              if (!isCreatedFamily) {
                setPopupContent(strings.REGISTER_YOUR_FAMILY_FIRST);
                setShowPopup(true);
                return;
              }
              back();
            }}
          />
          <div className="userInfo">
            <img className="userInfoImg" src={userInfoSVG} alt="userInfo" />
            <input
              type="text"
              value={userInfo.firstName}
              onChange={(e) => {
                // console.log(e.target.value);
                setUserInfo({ ...userInfo, firstName: e.target.value });
              }}
              className="firstNameTxt"
            />
            <select
              className="relationSelect"
              value={userInfo.relation}
              onChange={(e) => {
                setUserInfo({ ...userInfo, relation: e.target.value });
              }}
            >
              <option value="">انتخاب کنید</option>
              {RELATIONS.map((relation) => (
                <option value={relation}>{relation}</option>
              ))}
            </select>
            <input
              type="text"
              value={userInfo.lastName}
              onChange={(e) => {
                // console.log(e.target.value);
                setUserInfo({ ...userInfo, lastName: e.target.value });
              }}
              className="lastNameTxt"
            />
            <button
              className="addMemberBtn"
              onClick={() => {
                setActivePage("addMember");
                setMember(memberInitialVal);
              }}
            ></button>
            <img
              src={acceptSVG}
              className="saveAllBtn"
              alt="saveMemberBtn"
              onClick={async () => {
                try {
                  // const member = members[0];
                  // const phone = member.phoneNumber
                  //   ? `${member.code}${member.phoneNumber}`
                  //   : `${store.get("otp-number")}_${0}`;
                  // console.log(phone);
                  // return;
                  if (members.length === 0 && userInfo.relation !== "تنهام") {
                    setPopupContent("هنوز اعضای خانواده رو وارد نکردی.");
                    setShowPopup(true);
                    return;
                  }
                  await getStatus();
                  let createFamilyRes, familyId;
                  if (getFamilyTags().length === 0) {
                    createFamilyRes = await createFamily(
                      userInfo.firstName,
                      userInfo.lastName,
                      userInfo.relation,
                      userInfo.lastName,
                      getParameterByName("referral", window.location.href)
                    );
                    familyId = createFamilyRes.familyId;
                  } else {
                    familyId = getMyFirstFamilyId();
                  }
                  store.set("isCreatedFamily", true);
                  for (let index = 0; index < members.length; index++) {
                    const member = members[index];
                    const codeWithoutPlus =
                      member.code && member.code.replace("+", "");
                    const phone = member.phoneNumber
                      ? `${codeWithoutPlus}${member.phoneNumber}`
                      : `${store.get("otp-number")}_${index}`;
                    await addMember(
                      familyId,
                      member.relation,
                      phone,
                      member.firstName,
                      member.lastName
                    );
                  }
                  store.set("familyStatus", "created");
                  await getStatus();
                  store.set(
                    "family",
                    JSON.stringify({ lastname: userInfo.lastName })
                  );
                  // setPopupContent(strings.REGISTER_DONE_SUCCESSFULLY);
                  setPopupContent(<img src={createHomeOkSVG} />);
                  setShowPopup(true);
                  setPopupCloseCB(() => {
                    back();
                  });

                  // onClose();
                } catch (error) {
                  console.error(error);
                  // setPopupContent(strings.REGISTER_FAILED);
                  setPopupContent(<img src={createHomeFailedSVG} />);
                  setShowPopup(true);
                }
              }}
            />
            <div className="membersList">
              {members &&
                members.map((memberTmp, index) => (
                  <div className="member">
                    <div>
                      <span>
                        {memberTmp.firstName} {memberTmp.lastName}
                      </span>
                      <span
                        onClick={() => {
                          setActiveMember(index);
                          setMember(members[index]);
                          setActivePage("addMember");
                          console.log(index, members);
                        }}
                      >
                        <img className="editBtn" src={editSVG} alt="edit" />
                      </span>

                      <span
                        onClick={() => {
                          removeMember(index);
                        }}
                      >
                        <img
                          className="deleteBtn"
                          src={deleteSVG}
                          alt="delete"
                        />
                      </span>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      );
    case "addMember":
      return (
        <div className="khunatoBesaz-page addMember">
          <img src={bgBorder} alt="bgBorder" />
          <BackBtn
            onClick={() => {
              console.log("1");
              setActivePage("main");
            }}
          />
          <div className="userInfo memberInfo">
            <img className="userInfoImg" src={addMemberSVG} alt="addMember" />
            <input
              type="text"
              // ref={firstNameRef}
              value={member.firstName}
              onChange={(e) => {
                setMember({ ...member, firstName: e.target.value });
              }}
              className="firstNameTxt"
            />
            <select
              className="relationSelect"
              value={member.relation}
              onChange={(e) => {
                console.log(e);
                setMember({ ...member, relation: e.target.value });
              }}
            >
              <option value="">انتخاب کنید</option>
              {RELATIONS.map((relation) => (
                <option value={relation}>{relation}</option>
              ))}
            </select>
            <input
              type="text"
              ref={lastNameRef}
              className="lastNameTxt"
              value={member.lastName}
              onChange={(e) => {
                setMember({ ...member, lastName: e.target.value });
              }}
            />
            {/* <input
              type="number"
              ref={phoneNumberRef}
              className="phoneNumberTxt"
              value={member.phoneNumber}
              onChange={(e) => {
                setMember({ ...member, phoneNumber: e.target.value });
              }}
            /> */}
            <OTPTextBox
              className="phoneNumberTxt"
              code={member.code}
              phoneNumber={member.phoneNumber}
              setCode={(c) => {
                setMember({ ...member, code: c });
              }}
              setPhoneNumber={(p) => {
                // console.log(p);
                setMember({ ...member, phoneNumber: p });
              }}
            />
            <img
              src={plusSVG}
              className="saveMemberBtn"
              alt="saveMemberBtn"
              onClick={() => {
                const currentActive = activeMember;

                const cp = [...members];
                if (typeof cp[currentActive] !== "undefined")
                  setActiveMember(members.length);
                else setActiveMember(members.length + 1);

                cp[currentActive] = member;
                setMembers(cp);
                setMember(memberInitialVal);
                setActivePage("main");
                console.log(member);
                console.log(cp);
              }}
            />
          </div>
        </div>
      );
    default:
      break;
  }
};

export default KhunatoBesaz;
