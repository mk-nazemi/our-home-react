import React, { useEffect } from "react";
import BackBtn from "../../components/backBtn";
import { transitions, pages } from "../../constants";
import { reverseIt } from "../../tools/lottieHelpers";
const Filmamoon = ({ state, setState, runTransition, gotoPage }) => {
  useEffect(() => {
    console.log("run baziamoon transition");
    const t = transitions.filmamoon;
    runTransition(t);
  }, []);

  return (
    <div className="filmamoon-page">
      <BackBtn
        onClick={() => {
          const reverse = reverseIt({
            transition: transitions.filmamoon,
            onComplete: () => gotoPage({ page: pages.home })
          });
          runTransition(reverse);
        }}
      />

      <button
        className="khodemuniamoonBtn"
        onClick={() => {
          gotoPage({ page: pages.khodemuniamoon });
        }}
      ></button>

      <button
        className="movieLoveeBtn"
        onClick={() => {
          gotoPage({ page: pages.movieLovee });
        }}
      ></button>
    </div>
  );
};

export default Filmamoon;
