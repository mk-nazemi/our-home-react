import React, { useEffect, useState } from "react";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import store from "store";
import BackBtn from "../../components/backBtn";
import Moon from "../../components/Moon";
import Confirm from "../../components/Confirm";
import { strings } from "../../constants";
import deleteIcon from "../../resources/svg/deny.svg";
import { transitions, userSections, pages } from "../../constants";
import {
  likeFile,
  esfandFile,
  getFamilyFiles,
  getFilesAPI,
  deleteFile,
} from "../../api";
import AccessArea from "../../components/AccessArea";
import { getSelectedFamilyToVisit, getImgThumb } from "../../tools/funcs";
import { reverseIt } from "../../tools/lottieHelpers";
import Espand from "../../components/Espand";
const Axamoon = ({ state, setState, runTransition, gotoPage, page }) => {
  const [selectedImageIndex, setSelectedImageIndex] = React.useState(0);
  const [sliderIsOpen, setSliderIsOpen] = React.useState(false);

  const [editModeAttr, setEditModeAttr] = useState({
    on: false,
    fileId: null,
  });

  const [showConfirm, setShowConfirm] = useState(false);
  const [confirmContent, setConfirmContent] = useState(null);

  useEffect(() => {
    console.log("run baziamoon transition");
    const t = transitions.axamoon;
    runTransition(t);
  }, []);
  const [files, setFiles] = useState([]);
  async function getFiles() {
    const familyId = getSelectedFamilyToVisit();
    const files = await getFilesAPI(familyId, page.name);
    console.log(files);
    setFiles(files);
  }
  useEffect(() => {
    getFiles();
  }, []);

  console.log(files);
  return (
    <div className="axamoon-page">
      <BackBtn
        onClick={() => {
          const reverse = reverseIt({
            transition: transitions.axamoon,
            onComplete: () => gotoPage({ page: pages.home }),
          });
          runTransition(reverse);
        }}
      />
      {showConfirm && (
        <Confirm
          content={confirmContent}
          onConfirm={async () => {
            try {
              console.log("deleteing", editModeAttr);
              await deleteFile(editModeAttr.fileId);
              setShowConfirm(false);
              getFiles();
            } catch (e) {
              console.error(e);
            }
          }}
          onReject={() => {
            setShowConfirm(false);
          }}
        />
      )}
      <AccessArea
        upload={{
          fileType: page.uploadFileType,
          contentType: page.uploadFileType,
          familyId: Object.keys(store.get("familyTags"))[0],
          place: page.name,
          validTypes: page.validTypes,
          // type: "public",
          metadata: {},
          onClosed: () => {
            console.log("closed");
            getFiles();
          },
        }}
        gotoPage={gotoPage}
        page={page}
        onEditClicked={() => {
          console.log("editing");
          setEditModeAttr({
            ...editModeAttr,
            on: !editModeAttr.on,
          });
        }}
      />
      {sliderIsOpen && (
        <span className="imageSlider">
          <Lightbox
            mainSrc={files[selectedImageIndex].file.downloadUrl}
            nextSrc={
              files[(selectedImageIndex + 1) % files.length].file.downloadUrl
            }
            prevSrc={
              files[(selectedImageIndex + files.length - 1) % files.length].file
                .downloadUrl
            }
            onCloseRequest={() => setSliderIsOpen(false)}
            onMovePrevRequest={() =>
              setSelectedImageIndex(
                (selectedImageIndex + files.length - 1) % files.length
              )
            }
            onMoveNextRequest={() =>
              setSelectedImageIndex((selectedImageIndex + 1) % files.length)
            }
            imageCaption={
              files[selectedImageIndex].file.metadata ? (
                <span className="captionArea">
                  <span className="item captionMoon">
                    <Moon
                      onClick={async () => {
                        console.log(files, selectedImageIndex);
                        await likeFile(files[selectedImageIndex].id);
                        await getFiles();
                      }}
                      likeCount={
                        files[selectedImageIndex] &&
                        files[selectedImageIndex].likeCount
                      }
                    />
                  </span>
                  <span className="item captionEspand">
                    <Espand
                      onClick={() => {
                        esfandFile(files[selectedImageIndex].id);
                      }}
                    />
                  </span>
                  <span className="item captionTitle">
                    {files[selectedImageIndex].file.metadata.caption}
                  </span>
                </span>
              ) : (
                ""
              )
            }
            // discourageDownloads
          />
        </span>
      )}
      <div className="topImage">
        {files && files[selectedImageIndex] && (
          <img
            onClick={() => setSliderIsOpen(true)}
            src={files[selectedImageIndex].file.downloadUrl}
            alt="topImage"
          />
        )}
      </div>
      <div className="imagesGallery">
        {files &&
          files.map((fileData, index) => {
            // console.log(fileData);
            return (
              <div className="box">
                <img
                  onClick={(e) => setSelectedImageIndex(index)}
                  src={getImgThumb(fileData.file)}
                  alt="topImage"
                />
                {editModeAttr.on && (
                  <img
                    src={deleteIcon}
                    alt="delete"
                    className="deleteBtn"
                    onClick={() => {
                      console.log("deleting ", fileData);
                      setEditModeAttr({
                        ...editModeAttr,
                        fileId: fileData.id,
                      });
                      setConfirmContent(
                        <div>
                          <img
                            src={getImgThumb(fileData.file)}
                            className="previewImg"
                            alt="preview"
                          />
                          <div>{strings.DELETE_ARE_YOU_SURE}</div>
                        </div>
                      );
                      setShowConfirm(true);
                    }}
                  />
                )}
              </div>
            );
          })}
        {/* <div className="box">
          <img
            onClick={e => setSelectedImageIndex(1)}
            src={axamoon[1].link}
            alt="topImage"
          />
        </div>
        <div className="box">
          <img
            onClick={e => setSelectedImageIndex(2)}
            src={axamoon[2].link}
            alt="topImage"
          />
        </div>
        <div className="box">
          <img
            onClick={e => setSelectedImageIndex(3)}
            src={axamoon[3].link}
            alt="topImage"
          />
        </div>
        <div className="box">
          <img
            onClick={e => setSelectedImageIndex(4)}
            src={axamoon[4].link}
            alt="topImage"
          />
        </div> */}
        {/* <div className="box">
          <img
            onClick={e => setSelectedImageIndex(5)}
            src={axamoon[5].link}
            alt="topImage"
          />
        </div> */}
      </div>
      <div className="actionsArea">
        <span className="moonArea">
          <Moon
            onClick={async () => {
              console.log(files, selectedImageIndex);
              await likeFile(files[selectedImageIndex].id);
              await getFiles();
            }}
            likeCount={
              files[selectedImageIndex] && files[selectedImageIndex].likeCount
            }
          />
          <Espand
            onClick={() => {
              esfandFile(files[selectedImageIndex].id);
            }}
          />
        </span>
      </div>
    </div>
  );
};

export default Axamoon;
