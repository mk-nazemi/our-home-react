import React, { useEffect } from "react";
import { transitions, pages } from "../../constants";

const Splash = ({ runTransition, gotoPage }) => {
  useEffect(() => {
    const t = transitions.splash;
    t.callbacks = [];
    const onCompleteCallback = () => {
      console.log(pages.phoneOtp);
      gotoPage({ page: pages.phoneOtp });
    };
    t.callbacks.push({
      event: "onComplete",
      run: onCompleteCallback
    });
    runTransition(t);
  }, []);
  return <div className="splash-page"></div>;
};

export default Splash;
