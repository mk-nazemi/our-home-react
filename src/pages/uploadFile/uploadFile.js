import React, { useState } from "react";
import SearchArea from "../../components/SearchArea";
import { pages, transitions } from "../../constants";
import LottieRunner from "../../components/LottieRunner";
import * as animationData from "./resources/lottieFiles/upload.json.js";
const uploadFile = ({
  onClose,
  onItemSelected,
  setState,
  runTransition
}) => {
  const [transition, setTransition] = useState(
    transitions["start-to-phone-otp"]
  );
  return (
    <div className="upload-page">
      <LottieRunner
        animationData={animationData}
        transition={transition}
        clearTransition={() => {
          setTransition(null);
        }}
        speed={1}
        width={"100%"}
        height={"100%"}
      />
    </div>
  );
};

export default uploadFile;
