import React, { useState, useEffect } from "react";
import store from "store";
import { transitions, pages } from "../../constants";
import { RUN_MODES, runMode } from "../../config";
import { submitOtp, requestOtp, getStatus } from "../../api";
import FixedErrMsgArea from "../../components/FixedErrMessageArea";
const OTPVerify = ({ state, setState, runTransition, gotoPage }) => {
  const otpCodeTxtRef = React.createRef();
  const [error, setError] = React.useState("");
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    // clearPersistStates();
    const t = transitions.phoneVerify;
    runTransition(t);
  }, []);
  return (
    <div className="otp-verify-page">
      <input
        ref={otpCodeTxtRef}
        type="number"
        id="otp-code"
        maxLength="5"
        autoComplete="off"
      />
      <input
        disabled={loading}
        type="button"
        id="otp-submit"
        onClick={async () => {
          try {
            setLoading(true);
            const introPage = pages.intro;
            // if (runMode === RUN_MODES.DEBUG) {
            //   gotoPage({ page: introPage });
            //   setLoading(false);
            //   return;
            // }
            console.log("otp submit clicked");
            let code = otpCodeTxtRef.current.value;
            console.log("otp code is", code);
            const res = await submitOtp(code);
            store.set("isCreatedFamily", res.isCreatedFamily);
            const userStatus = await getStatus();
            // const invitations = await getFamilyRequests();
            // store.set("familyInvitations", invitations.request);
            gotoPage({ page: introPage });
            setLoading(false);
          } catch (error) {
            console.error(error);
            setError(error);
            setLoading(false);
          }
        }}
      />
      <button
        className="resendOTPBtn"
        onClick={async () => {
          console.log("resendBtn clicked");
          try {
            await requestOtp(store.get("otp-number"));
          } catch (error) {
            setError(error);
          }
        }}
      ></button>
      <FixedErrMsgArea msg={error} />
    </div>
  );
};

export default OTPVerify;
