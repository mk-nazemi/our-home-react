import React, { useState, useEffect } from "react";
import jwtDecode from "jwt-decode";
// import store from "store";
// import jwtDecode from "jwt-decode";
import BackBtn from "../../components/backBtn";
import FamilyMembersImageBox from "../../components/familyMembersImageBox";
import { transitions, pages } from "../../constants";
import { searchFamilyById } from "../../api";
import Popup from "../../components/Popup";

import { getSelectedFamilyToVisit } from "../../tools/funcs";
const store = require("store");

const DoorBell = ({ state, setState, runTransition, gotoPage }) => {
  const decodedToken = jwtDecode(store.get("userToken"));
  const isBen = decodedToken.uid === "989121234880";
  const currentVisitingHomeIsBen = getSelectedFamilyToVisit() === "OwftkYG2sZ";

  const [familyInfo, setFamilyInfo] = useState({});
  const [loadFamilyMembersBox, setLoadFamilyMembersBox] = useState(false);
  const [animation1State, setAnimation1State] = useState(0);

  useEffect(() => {
    console.log(familyInfo);
    if (animation1State === 1) return;
    if (animation1State === 2) {
      setLoadFamilyMembersBox(true);
      return;
    }
    setAnimation1State(1); // running
    const t = transitions.doorBell;
    console.log("doorbell transition", t);
    t.callbacks = [
      {
        event: "onComplete",
        run: () => {
          setAnimation1State(2);
        },
      },
    ];
    runTransition(t);
  }, [familyInfo, animation1State, loadFamilyMembersBox]);

  // useEffect(() => {
  //   checkFamilyLoaded();
  // }, [checkFamilyLoaded, familyInfo]);

  useEffect(() => {
    async function getFamilyInfo() {
      const familyId = getSelectedFamilyToVisit();
      const FI = await searchFamilyById(familyId);
      console.log("familyInfo", FI);
      setFamilyInfo(FI);
    }
    getFamilyInfo();
  }, []);

  const [displayImages, setDisplayImages] = React.useState(true);
  const [showPopup, setShowPopup] = useState(false);
  const [popupContent, setPopupContent] = useState("");

  // console.log("displayImages", displayImages);
  return (
    <div className="DoorBell-page">
      <BackBtn
        onClick={() => {
          gotoPage({ page: pages.intro });
        }}
      />
      <div className="pelak">{familyInfo.familyPelak}</div>
      <div className="bellName">{familyInfo.familyTitle}</div>
      <div
        className="bellBtn"
        onClick={async () => {
          if (currentVisitingHomeIsBen && !isBen) {
            setPopupContent(
              "در حال آماده کردن خونه مون برای تولد بارانا هستیم"
            );
            setShowPopup(true);
            return;
          }
          gotoPage({ page: pages.home });
        }}
      ></div>
      <div className="gallery">
        <div className="galleryList">
          {/* {state.introHotSelectedId === null &&
            familyInfo.membersX &&
            familyInfo.membersX.slice(0, 4).map((item, index) => (
              <div key={index} className={`box imgArea box${index + 1}`}>
                <img
                  src={item.link}
                  alt={`hot${index + 1}`}
                  onClick={() => {
                    setState({ introHotSelectedId: index });
                    runTransition(
                      transitions["doorbell-gallery-to-doorbell-singleHot"]
                    );
                  }}
                />
              </div>
            ))} */}
        </div>
        {/* {state.introHotSelectedId !== null && (
          <div className="singleItem">
            <img
              src={images[state.introHotSelectedId].link}
              alt="singleHotSelected"
            />
          </div>
        )} */}
      </div>

      {loadFamilyMembersBox && (
        <FamilyMembersImageBox familyInfo={familyInfo} onClose={() => {}} />
      )}
      {showPopup && (
        <Popup
          content={popupContent}
          onClose={() => {
            setShowPopup(false);
          }}
        />
      )}
    </div>
  );
};

export default DoorBell;
