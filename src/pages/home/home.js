import React, { useEffect, useState } from "react";
import store from "store";
import backBtn from "../../components/backBtn";
import SearchIcon from "../../components/searchIcon";
import InviteOthersIcon from "../../components/inviteOthersIcon";
import InvitationRequestsIcon from "../../components/InvitationRequestsIcon";
import HomeIcon from "../../components/HomeIcon";
import { transitions, pages, strings } from "../../constants";
import BackBtn from "../../components/backBtn";
import didemoonBazdidemoonIcon from "../../resources/svg/didemoon-bazdidemoon.svg";
import didemoonIcon from "../../resources/svg/didemoon.svg";
import bazdidemoonIcon from "../../resources/svg/bazdidemoon.svg";
import * as didemoonBazdidemoonAnimation from "../../resources/lottieFiles/didemoon bazdidemoon.json";
import { authenticate, familyConnectionRequest } from "../../api";
import {
  getSelectedFamilyToVisit,
  gotoMyHome,
  isItMyHome,
  hasPermissionTo,
  isCreatedFamily,
  getSelectedFamilyTitleToVisit,
  toUpperFirstLetter,
} from "../../tools/funcs";
import SimpleLottieRunner from "../../components/SimpleLottieRunner";
import NavBar from "../../components/NavBar";

const Home = ({ state, setState, runTransition, gotoPage }) => {
  useEffect(() => {
    const t = transitions.home;
    runTransition(t);
  }, []);

  function renderElements(name) {
    return (
      <div
        id={name}
        onClick={() => {
          // const t = transitions[`homeTo${toUpperFirstLetter(name)}IconReverse`];
          // t.callbacks = [
          //   {
          //     event: "onEnterFrame",
          //     frame: t.to,
          //     run: () => {
          //       console.log("running callback", name, pages[name]);
          //       gotoPage({ page: pages[name] });
          //     }
          //   }
          // ];
          // runTransition(t);
          gotoPage({ page: pages[name] });
        }}
      ></div>
    );
  }
  const [loaded, setLoaded] = useState(false);

  const [play, setPlay] = useState(false);
  // const [isMyHomePage, setIsMyHomePage] = useState(!getSelectedFamilyToVisit());
  useEffect(() => {
    setLoaded(true);
    setTimeout(() => {
      setPlay(true);
    }, 2000);
    async function doAuth() {
      await authenticate();
    }
    doAuth();
  }, []);
  return (
    <div className="home-page">
      <SearchIcon gotoPage={gotoPage} />

      <InvitationRequestsIcon gotoPage={gotoPage} />
      <InviteOthersIcon gotoPage={gotoPage} />
      {!isItMyHome() && <HomeIcon gotoPage={gotoPage} />}

      {/* <img
        src={didemoonBazdidemoonIcon}
        className="didemoonBazdidemoon"
        alt="didemoonBazdidemoon"
      /> */}
      {isItMyHome() && (
        <div className="didemoonBazdidemoonAnimation">
          <SimpleLottieRunner
            animationData={didemoonBazdidemoonAnimation}
            isPaused={!play}
            isStopped={!play}
            delayForPlay={2000}
            speed={2}
            // loop
            // autoplay
            autoplay
            // speed={4}
            width={"100%"}
            height={"100%"}
          />
        </div>
      )}
      {!isItMyHome() && (
        <div className="headerFamilyName">
          {getSelectedFamilyTitleToVisit()}
        </div>
      )}
      <input
        type="button"
        className="didemoonBtn"
        onClick={() => gotoPage({ page: pages.didemoon })}
      />
      <input
        type="button"
        className="bazdidemoonBtn"
        onClick={() => gotoPage({ page: pages.bazdidemoon })}
      />
      <BackBtn
        onClick={() => {
          console.log(isItMyHome());
          gotoPage({ page: pages.intro });
          // if (!isItMyHome()) {

          //   // store.remove("currentVisitingHome");
          //   gotoMyHome();
          //   // setIsMyHomePage(true);
          // } else {
          //   gotoPage(pages.intro);
          //   // runTransition(transitions["home-to-doorbell"]);
          // }
        }}
      />
      {renderElements("baziamoon")}
      {renderElements("axamoon")}
      {renderElements("ahangamoon")}
      {renderElements("filmamoon")}
      {renderElements("ashpazkhunamoon")}
      {renderElements("taghvimemoon")}
      {renderElements("honarhamoon")}
      {renderElements("ketabkhunamoon")}
      {renderElements("khaterehamoon")}
      {renderElements("panjerehamoon")}
      {renderElements("shabamoon")}
      {renderElements("mehmooniamoon")}
      {renderElements("chamedoon")}
    </div>
  );
};

export default Home;
