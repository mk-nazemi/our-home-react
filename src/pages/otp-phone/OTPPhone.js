import React, { useEffect } from "react";
import store from "store";
import { transitions, pages } from "../../constants";
// import { RUN_MODES, runMode } from "../../config";
import { requestOtp } from "../../api";
import FixedErrMsgArea from "../../components/FixedErrMessageArea";
import { getParameterByName } from "../../tools/funcs";
import OTPTextBox from "../../components/optTextBox";
const OTPPhone = ({ state, setState, runTransition, gotoPage }) => {
  const [error, setError] = React.useState("");
  const [code, setCode] = React.useState();
  const [phoneNumber, setPhoneNumber] = React.useState();
  useEffect(() => {
    // clearPersistStates();
    const t = transitions.phoneOtp;
    runTransition(t);
  }, []);
  return (
    <div className="otp-phone-page">
      <OTPTextBox
        code={code}
        phoneNumber={phoneNumber}
        setCode={setCode}
        setPhoneNumber={setPhoneNumber}
      />
      <input
        type="button"
        id="otp-submit"
        onClick={async () => {
          // return console.log(code, phoneNumber);
          const otpVerifyPage = pages.phoneVerify;
          // if (runMode === RUN_MODES.DEBUG) {
          //   gotoPage({ page: otpVerifyPage });
          //   return;
          // }
          try {
            console.log("otp submit clicked", phoneNumber);
            store.set("otp-number", `${code}${phoneNumber}`);
            const referral = getParameterByName(
              "referral",
              window.location.href
            );
            await requestOtp(code, phoneNumber, referral);
            gotoPage({ page: otpVerifyPage });
          } catch (error) {
            console.log(error);
            setError(error);
            switch (error) {
              case "ALREADY REQUESTED!":
                gotoPage({ page: otpVerifyPage });
                break;
              default:
                break;
            }
          }
        }}
      />
      <FixedErrMsgArea msg={error} />
    </div>
  );
};

export default OTPPhone;
