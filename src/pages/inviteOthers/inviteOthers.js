import React, { useState } from "react";
import BackBtn from "../../components/backBtn";
import { actionOnFamilyInvitation, getFamilyRequests } from "../../api/index";
import store from "store";
import TextBox from "../../components/TextBox";
import SubmitButton from "../../components/SubmitButton";
import { pages, transitions } from "../../constants";
import bgBorder from "../../resources/svg/big-border.svg";
import addIcon from "../../resources/svg/plus.svg";
import removeIcon from "../../resources/svg/add.svg";
// import phoneNumberSVG from "../../resources/svg/phoneNumber.svg";
import { getInviteLink } from "../../api/index";
import { correctPhoneNumber } from "../../tools/funcs";
import OTPTextBox from "../../components/optTextBox";
const requestsData = store.get("familyInvitations");
const InviteOthers = ({
  onClose,
  onItemSelected,
  setState,
  runTransition,
  gotoPage,
}) => {
  const [numbers, setNumbers] = useState([]);
  const [inviteLink, SetInviteLink] = useState(null);
  return (
    <div className="invitationRequests-page">
      <img src={bgBorder} alt="bgBorder" />
      <BackBtn onClick={() => onClose()} />

      {inviteLink && (
        <textarea type="text" className="inviteLinkTxt" value={inviteLink}>
          {inviteLink}
        </textarea>
      )}
      {/* <img src={phoneNumberSVG} alt='phoneNumberSVG' /> */}
      <img
        src={addIcon}
        alt="addIcon"
        className="addNumberIcon"
        onClick={() => {
          setNumbers([
            ...numbers,
            {
              code: "",
              phoneNumber: "",
            },
          ]);
        }}
      />
      <div className="numbersArea">
        {numbers.map((number, index) => (
          <div>
            <OTPTextBox
              className="phoneNumberTxt"
              code={number.code}
              phoneNumber={number.phoneNumber}
              setCode={(c) => {
                const cp = [...numbers];
                cp[index].code = c;
                setNumbers(cp);
              }}
              setPhoneNumber={(p) => {
                // console.log(p);
                const cp = [...numbers];
                cp[index].phoneNumber = p;
                setNumbers(cp);
              }}
            />
            {/* <TextBox
              type="number"
              value={numbers[index]}
              onChange={e => {
                const cp = [...numbers];
                cp[index] = e.target.value;
                setNumbers(cp);
              }}
            /> */}
            <img
              src={removeIcon}
              alt="removeIcon"
              className="removeIcon"
              onClick={() => {
                const cp = [...numbers];
                cp.splice(index, 1);
                setNumbers(cp);
              }}
            />
          </div>
        ))}
      </div>
      <SubmitButton
        onClick={async () => {
          const validNumbers = numbers
            .filter((number) => {
              console.log(number.code, number.phoneNumber);
              return number.code && number.phoneNumber;
            })
            .map(
              (number) => `${number.code.replace("+", "")}${number.phoneNumber}`
            );
          const res = await getInviteLink(validNumbers);
          SetInviteLink(res.inviteLink);
        }}
      />
    </div>
  );
};

export default InviteOthers;
