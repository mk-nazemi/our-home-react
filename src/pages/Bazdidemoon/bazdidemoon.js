import React from "react";
import BackBtn from "../../components/backBtn";
import { transitions, pages } from "../../constants";
import BG from "../../resources/svg/border2.svg";
// var classNames = require("classnames");
const Bazdidemoon = ({ state, setState, runTransition, gotoPage }) => {
  return (
    <div className="bazdidemoon-page">
      <BackBtn
        onClick={() => {
          gotoPage({ page: pages.home });
        }}
      />
      <img src={BG} alt="bg" />
    </div>
  );
};

export default Bazdidemoon;
