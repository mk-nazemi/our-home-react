import React, { useState, useEffect } from "react";
import store from "store";
import BackBtn from "../../components/backBtn";
import LottieRunner from "../../components/LottieRunner";
import * as playPauseLottieData from "../../resources/lottieFiles/playpause.json";
import * as moonData from "../../resources/lottieFiles/Moon V04.json";
import * as esfandData from "../../resources/lottieFiles/Espand V04.json";
import LikeIcon from "../../resources/svg/moon.svg";
import EspandIcon from "../../resources/svg/espand.svg";
import { Player, BigPlayButton, ControlBar, Shortcut } from "video-react";
import { transitions, pages } from "../../constants";
import Seeker from "../../components/Seeker";
import AccessArea from "../../components/AccessArea";

import classNames from "classnames";
import { likeFile, esfandFile, seenFile, getFilesAPI } from "../../api";
import {
  getSelectedFamilyToVisit,
  prettyNum,
  generateMemberImage,
  getSelectedFamilyToVisitInfo,
} from "../../tools/funcs";
import { reverseIt } from "../../tools/lottieHelpers";

const Ahangamoon = ({ state, setState, runTransition, gotoPage, page }) => {
  const familyInfo = getSelectedFamilyToVisitInfo();
  useEffect(() => {
    console.log("run Ahangamoon transition");
    const t = transitions.ahangamoon;
    runTransition(t);
  }, []);

  const [playLists, setPlayLists] = useState([]);
  const [files, setFiles] = useState([]);
  async function getFiles() {
    const familyId = getSelectedFamilyToVisit();
    const files = await getFilesAPI(familyId, page.name);
    console.log(files);
    setFiles(files);
    const playList1 = {
      playList: files,
    };
    const familyPlayLists = files.reduce((acc, fileObject) => {
      console.log(fileObject);
      if (!acc[fileObject.file.owner._id]) acc[fileObject.file.owner._id] = {};
      if (!acc[fileObject.file.owner._id].playList)
        acc[fileObject.file.owner._id].playList = [];
      acc[fileObject.file.owner._id].playList.push(fileObject);
      return acc;
    }, []);
    setPlayLists(familyPlayLists);
    if (familyPlayLists[0]) setPlayList(familyPlayLists[0]);
  }
  useEffect(() => {
    getFiles();
  }, []);
  const gotoNextMusic = () => {
    if (!playerRef || !playerRef.current) return;
    playerRef.current.pause();
    let newIndex = playListIndex + 1;
    // seenFile(playList[newIndex].id);
    setPlayListIndex(newIndex % playList.length);
    // playerRef.current.load();
    // playerRef.current.seek(0);
    // playerRef.current.play();
  };

  const gotoPrevMusic = () => {
    if (!playerRef || !playerRef.current) return;
    playerRef.current.pause();
    if (playListIndex === 0) {
      setPlayListIndex(playList.length - 1);
      // playerRef.current.load();
      // playerRef.current.play();
      return;
    }
    // console.log(playListIndex);
    setPlayListIndex((playListIndex - 1) % playList.length);
    // playerRef.current.load();
    // playerRef.current.play();
  };

  const changePerson = (userId) => {
    if (!playerRef || !playerRef.current) return;
    if (!playLists[userId]) return;
    setCurrentUserIndex(userId);
    playerRef.current.pause();
    playerRef.current.seek(0);
    console.log(playLists, userId);
    setPlayList(playLists[userId].playList);
  };

  const [currentUserIndex, setCurrentUserIndex] = React.useState(0);
  const [playList, setPlayList] = React.useState(
    playLists[currentUserIndex] && playLists[currentUserIndex].playList
      ? playLists[currentUserIndex].playList
      : []
  );
  const [playListIndex, setPlayListIndex] = React.useState(0);

  const [audioDuration, setAudioDuration] = React.useState(0);
  const [currentTime, setCurrentTime] = React.useState(0);
  const [playPauseTransition, setPlayPauseTransition] = React.useState(null);
  const [moonTransition, setMoonTransition] = React.useState(null);
  const [cheshmzakhmTransition, setCheshmzakhmTransition] = React.useState(
    null
  );
  const playerRef = React.createRef();
  // console.log("playListIndex", playListIndex);
  React.useEffect(() => {
    if (!playerRef || !playerRef.current) return;
    playerRef.current.subscribeToStateChange((state, prevState) => {
      setCurrentTime(state.currentTime);
      setAudioDuration(state.duration);
      if (state.ended) {
        gotoNextMusic();
      }
      if (
        playerRef.current &&
        prevState.currentSrc != null &&
        state.currentSrc !== prevState.currentSrc
      ) {
        playerRef.current.play();
        seenFile(playList[playListIndex].id);
        // setPlayPauseTransition({ from: 56, to: 120 });
      }
      // console.log(playerRef);
      // console.log(state.currentTime);
      // console.log(currentTime);

      if (state.paused !== prevState.paused) {
        // console.log("h1");
        if (!state.paused) {
          // console.log("h2");
          // setPlayPauseTransition(null);
          setPlayPauseTransition({ from: 56, to: 120 });
        } else {
          // console.log("h3");
          // setPlayPauseTransition(null);
          setPlayPauseTransition({ from: 0, to: 56 });
        }
      }
    });
  }, [playerRef, state.duration]);
  // console.log(playLists);
  // console.log(playList);
  return (
    <div className="ahangamoon-page">
      <BackBtn
        onClick={() => {
          const reverse = reverseIt({
            transition: transitions.ahangamoon,
            onComplete: () => gotoPage({ page: pages.home }),
          });
          runTransition(reverse);
        }}
      />

      <AccessArea
        upload={{
          fileType: page.uploadFileType,
          contentType: page.uploadFileType,
          familyId: Object.keys(store.get("familyTags"))[0],
          place: page.name,
          validTypes: page.validTypes,
          // type: "public",
          metadata: {},
          onClosed: () => {
            console.log("closed");
            getFiles();
          },
        }}
        page={page}
      />

      <div className="imagesArea">
        {familyInfo.members.map((member, index) => (
          <div className={`box box${index + 1}`}>
            <img
              src={generateMemberImage(familyInfo.familyId, member.memberId)}
              onError={(e) => {
                console.log("image not found error", e);
                e.target.style.display = "none";
              }}
              alt={member.firstname}
              onClick={() => {
                console.log("clicked");
                changePerson(member.memberId);
              }}
            />
            <span onClick={() => changePerson(member.memberId)}>
              {member.firstname}
            </span>
          </div>
        ))}
      </div>
      <div style={{ opacity: 0, width: 0, height: 0 }}>
        <Player
          playsInline
          ref={playerRef}
          src={
            playList[playListIndex] && playList[playListIndex].file.downloadUrl
          }
        >
          <ControlBar disabled />
          <BigPlayButton disabled />
          <Shortcut clickable={false} />
        </Player>
      </div>
      <div className="seekerArea">
        <Seeker
          noSeekerLine={true}
          duration={audioDuration}
          setTime={(newTime) => {
            if (!playerRef || !playerRef.current) return;
            playerRef.current.seek(newTime);
          }}
          currentTime={currentTime}
        />
      </div>

      <div className="actionsArea">
        <div
          className={classNames("likeEffect", {
            hideDisplay: moonTransition === null,
          })}
        >
          <LottieRunner
            setState={(state) => {
              // this.setState(state);
            }}
            speed={3}
            animationData={moonData}
            transition={moonTransition}
            clearTransition={() => {
              setMoonTransition(null);
            }}
          />
        </div>

        <div
          className={classNames("cheshmzakhmEffect", {
            hideDisplay: cheshmzakhmTransition === null,
          })}
        >
          <LottieRunner
            className={classNames({
              hideDisplay: cheshmzakhmTransition === null,
            })}
            setState={(state) => {
              // this.setState(state);
            }}
            speed={1}
            animationData={esfandData}
            transition={cheshmzakhmTransition}
            clearTransition={() => {
              setCheshmzakhmTransition(null);
            }}
          />
        </div>

        <span
          onClick={async () => {
            console.log("clicked");
            setMoonTransition({ from: 0, to: 130 });
            await likeFile(playList[playListIndex].id);
            // await getFiles();
          }}
        >
          <span className="moonCount">
            {playList[playListIndex] &&
              playList[playListIndex].likeCount &&
              prettyNum(playList[playListIndex].likeCount)}
          </span>
          <img
            src={LikeIcon}
            className="moonIcon"
            alt="moon"
            width="30%"
            height="30%"
          />
        </span>
        <span
          onClick={() => {
            setCheshmzakhmTransition({ from: 0, to: 38 });
            esfandFile(playList[playListIndex].id);
          }}
        >
          <img
            src={EspandIcon}
            className="espandIcon"
            alt="moon"
            width="30%"
            height="30%"
          />
        </span>
      </div>
      <div className="controls">
        <div
          className="prevBtn"
          onClick={() => {
            console.log("prevBtn clicked");
            // if (playListIndex === 0) {
            //   setPlayListIndex(playList.length - 1);
            //   return;
            // }
            // // console.log(playListIndex);
            // setPlayListIndex((playListIndex - 1) % playList.length);
            // console.log(playListIndex);
            gotoPrevMusic();
          }}
        ></div>
        <div
          className="nextBtn"
          onClick={() => {
            console.log("nextBtn clicked");
            // console.log(playListIndex);
            // setPlayListIndex((playListIndex + 1) % playList.length);
            gotoNextMusic();
            // console.log(playListIndex);
          }}
        ></div>
        <div
          className="audioPlayPauseBtn"
          onClick={() => {
            if (!playList || !playList[playListIndex]) return;
            console.log("clicked", playList, playListIndex);
            if (!playerRef || !playerRef.current) return;
            const { player } = playerRef.current.getState();
            if (player.paused) {
              playerRef.current.play();
              setPlayPauseTransition({ from: 56, to: 120 });
              seenFile(playList[playListIndex].id);
            } else {
              playerRef.current.pause();
              setPlayPauseTransition({ from: 0, to: 56 });
            }
          }}
        >
          <LottieRunner
            setState={(state) => {
              // this.setState(state);
            }}
            animationData={playPauseLottieData}
            transition={playPauseTransition}
            clearTransition={() => {
              setPlayPauseTransition(null);
            }}
            startFrame={56}
          />
        </div>
      </div>

      <div className="playerInfo">
        {audioDuration && (
          <div class="duration">
            <span>مدت زمان: </span>
            <span>
              {isNaN(parseInt(audioDuration / 60))
                ? ""
                : parseInt(audioDuration / 60)}
            </span>
            <span>دقیقه</span>
          </div>
        )}

        <div class="playingIndex">
          <span>{playListIndex + 1}</span>/<span>{playList.length}</span>
        </div>
      </div>
    </div>
  );
};

export default Ahangamoon;
