import React, { useState, useEffect } from "react";
import BackBtn from "../../components/backBtn";
import {
  actionOnFamilyInvitation,
  getFamilyRequests,
  getFamilyConnectionRequests,
  familyActionOnConnectionRequest,
  getStatus,
} from "../../api/index";
import acceptSVG from "../../resources/svg/accept-btn.svg";
import rejectSVG from "../../resources/svg/reject-btn.svg";
import store from "store";
import SearchArea from "../../components/SearchArea";
import {
  pages,
  transitions,
  strings,
  PERMISSION_LEVELS,
} from "../../constants";
import bgBorder from "../../resources/svg/big-border.svg";
import Loading from "../../components/Loading";
import GiveFamilyPermission from "../../components/GiveFamilyPermission";
import { getPendingInvitations } from "../../tools/funcs";
const InvitationRequests = ({
  onClose,
  onItemSelected,
  setState,
  runTransition,
}) => {
  async function updateRequests(closeOnNoReq = true) {
    console.log("here");
    const status = await getStatus();
    setFamilyRequests(status.pendingInvitations);
    const fc = await getFamilyConnectionRequests();
    setFamilyConnectionRequests(fc);

    console.log(status.pendingInvitations, fc);
    if (
      closeOnNoReq &&
      status.pendingInvitations.length === 0 &&
      fc.length === 0
    ) {
      onClose();
    }
  }

  useEffect(() => {
    updateRequests(false);
  }, []);
  const [familyRequests, setFamilyRequests] = useState([]);
  const [familyConnectionRequests, setFamilyConnectionRequests] = useState([]);
  console.log(familyRequests, familyConnectionRequests);
  const [permissions, setPermissions] = useState({});
  return (
    <div
      className="invitationRequests-page"
      // style={{ backgroundImage: `url(${bgBorder})` }}
    >
      {/* <img src={bgBorder} alt="bgBorder" /> */}
      <BackBtn onClick={() => onClose()} />

      <div className="requestsList">
        {familyRequests &&
          familyRequests.length > 0 &&
          familyRequests.map((request, index) => (
            <div
              className="request joinToFamily"
              // style={{ backgroundImage: `url(${bgBorder})` }}
            >
              <div>
                <span>{strings.JOIN_TO_FAMILY_REQUEST_FROM}</span>
                <span>{request.familyTitle}</span>
              </div>

              <div>
                <span>{strings.YOUR_ROLE_IN_FAMILY}</span>
                <span>{request.role}</span>
              </div>

              <div>
                <span>{strings.YOUR_FULL_NAME}</span>
                <span>{`${request.firstname} ${request.lastname}`}</span>
              </div>

              <span className="actionsArea">
                <img
                  src={acceptSVG}
                  className="confirmOkBtn"
                  alt="confirmOkBtn"
                  onClick={async () => {
                    await actionOnFamilyInvitation(request.requestId, "accept");
                    await updateRequests();
                  }}
                />

                <img
                  src={rejectSVG}
                  className="rejectBtn"
                  alt="rejectBtn"
                  onClick={async () => {
                    await actionOnFamilyInvitation(request.requestId, "reject");
                    await updateRequests();
                  }}
                />
              </span>
            </div>
          ))}
        {familyConnectionRequests &&
          familyConnectionRequests.length > 0 &&
          familyConnectionRequests.map((request, index) => (
            <div
              className="request"
              // style={{ backgroundImage: `url(${bgBorder})` }}
            >
              <span className="familyTitle">
                {request.inviterFirstname} {request.inviterLastname}
              </span>

              <GiveFamilyPermission
                onChange={(checkList) => {
                  const cp = { ...permissions };
                  cp[request.requestId] = checkList;
                  setPermissions(cp);
                }}
              />
              <span className="actionsArea">
                <img
                  src={acceptSVG}
                  className="confirmOkBtn"
                  alt="confirmOkBtn"
                  onClick={async () => {
                    // await familyConnectionRequest(request.requestId);
                    const permissionsMapped = Object.entries(
                      permissions[request.requestId]
                    ).reduce((result, entry) => {
                      if (entry[1].view)
                        result[entry[0]] = PERMISSION_LEVELS.VIEW;
                      return result;
                    }, {});
                    await familyActionOnConnectionRequest(
                      request.invitationId,
                      "accept",
                      permissionsMapped
                    );
                    await updateRequests();
                  }}
                />

                <img
                  src={rejectSVG}
                  className="rejectBtn"
                  alt="rejectBtn"
                  onClick={async () => {
                    await familyActionOnConnectionRequest(
                      request.invitationId,
                      "reject",
                      {}
                    );
                    await updateRequests();
                  }}
                />
              </span>
            </div>
          ))}
        {familyRequests.length === 0 &&
          familyConnectionRequests.length === 0 && (
            <div>هنوز درخواستی وجود ندارد</div>
          )}
        {/* {!requests && <Loading />} */}
      </div>
    </div>
  );
};

export default InvitationRequests;
