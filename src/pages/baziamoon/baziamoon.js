import React, { useEffect } from "react";
import BackBtn from "../../components/backBtn";
import store from "store";
import { transitions, pages } from "../../constants";
import { reverseIt } from "../../tools/lottieHelpers";

const Baziamoon = ({ state, gotoPage, runTransition }) => {
  useEffect(() => {
    console.log("run baziamoon transition");
    const t = transitions.baziamoon;
    runTransition(t);
  }, []);

  return (
    <div className="baziamoon-page">
      <BackBtn
        onClick={() => {
          const reverse = reverseIt({
            transition: transitions.baziamoon,
            onComplete: () => gotoPage({ page: pages.home })
          });
          runTransition(reverse);
        }}
      />

      <button
        className="dabernaGameStartBtn"
        onClick={() => {
          window.location.href = `https://daberna.serveapp.ir/MainPage/Register?token=${store.get(
            "refreshToken"
          )}`;
        }}
      />
    </div>
  );
};

export default Baziamoon;
