import React from "react";
import SearchArea from "../../components/SearchArea";
import { pages, transitions } from "../../constants";
const Search = ({ onItemSelected, back, runTransition }) => {
  return (
    <div className="search-page">
      <SearchArea
        onClose={() => {
          back();
        }}
        onItemSelected={() => {
          onItemSelected();
        }}
      />
    </div>
  );
};

export default Search;
