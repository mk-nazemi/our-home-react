import React from "react";
import BackBtn from "../../components/backBtn";
import classNames from "classnames";
import { Player, BigPlayButton, ControlBar, Shortcut } from "video-react";
import LottieRunner from "../../components/LottieRunner";
import * as uploadMsgPopup from "../../resources/lottieFiles/Pop up.json";

import { transitions } from "../../constants";
import { getUploadToken } from "../../api";
const Chalenjamoon = ({ state, setState, runTransition }) => {
  console.log("backto", state.backTo);
  const [popupTransition, setPopupTransition] = React.useState(null);
  const [showPopup, setShowPopup] = React.useState(false);
  const playerRef = React.createRef();
  return (
    <div
      className={classNames("chalenjamoon-page", {
        "back-to-home": state.backTo === "home"
      })}
    >
      <BackBtn
        onClick={() => {
          console.log("clicked");
          switch (state.backTo) {
            case "home":
              runTransition(transitions["chalenjamoon-to-home"]);
              break;
            default:
              runTransition(transitions["chalenjamoon-to-shabamoon"]);
              break;
          }
        }}
      />
      <div
        className="thisMusicLink"
        onClick={() => {
          playerRef.current.play();
        }}
      ></div>
      <div style={{ opacity: 0, width: 0, height: 0 }}>
        <Player
          playsInline
          ref={playerRef}
          src={"http://www.hochmuth.com/mp3/Haydn_Cello_Concerto_D-1.mp3"}
        >
          <ControlBar disabled />
          <BigPlayButton disabled />
          <Shortcut clickable={false} />
        </Player>
      </div>
      <div className={classNames("popupMsg", { hide: !showPopup })}>
        <LottieRunner
          setState={state => {
            // this.setState(state);
          }}
          animationData={uploadMsgPopup}
          transition={popupTransition}
          clearTransition={() => {
            setPopupTransition(null);
          }}
        />
        <div
          class="confirmBtn"
          onClick={() => {
            setShowPopup(false);
          }}
        ></div>
      </div>

      <div
        class="participantInChallengeBtn"
        onClick={async () => {
          try {
            const uploadToken = await getUploadToken();
            console.log("uploadToken", uploadToken);
            if (typeof androidApp === "undefined") {
              //in browser
              window.open(
                `https://generaluploader.mabeyn.ir/${uploadToken}`,
                "_blank"
              );
            } else {
              // in rubika
              setShowPopup(true);
              setPopupTransition({ from: 0, to: 225 });
            }
          } catch (error) {
            console.error(error);
          }
        }}
      ></div>
    </div>
  );
};

export default Chalenjamoon;
