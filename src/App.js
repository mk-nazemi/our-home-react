import React, { createRef } from "react";
import store from "store";
import OTPPhone from "./pages/otp-phone/OTPPhone";
import OTPVerify from "./pages/otp-verify/OTPVerify";
import DoorBell from "./pages/Door-bell/DoorBell";
import Home from "./pages/home/home";
import Shabamoon from "./pages/shabamoon/shabamoon";
import Chalenjamoon from "./pages/chalenjamoon/chalenjamoon";
import Ahangamoon from "./pages/Ahangamoon/ahangamoon";
import Axamoon from "./pages/Axamoon/axamoon";
import Filmamoon from "./pages/Filmamoon/filmamoon";
import Intro from "./pages/intro/intro";
import Khodemooniamoon from "./pages/Khodemooniamoon/khodemooniamoon";
import MovieLovee from "./pages/MovieLovee/movieLovee";
import Baziamoon from "./pages/baziamoon/baziamoon";
import Search from "./pages/search/search";
import KhunatoBesaz from "./pages/khunatoBesaz/khunatoBesaz";
import DidemoonPage from "./pages/Didemoon/didemoon";
import InvitationRequests from "./pages/invitationRequests/invitationRequests";
import InviteOthers from "./pages/inviteOthers/inviteOthers";
import SimpleLottieRunner from "./components/SimpleLottieRunner";
import Splash from "./pages/uploadFile/Splash";
// import LottieRunner from "./components/LottieRunner";
import { pages } from "./constants";

// import * as animationData from "./resources/lottieFiles/data V17.json";
// import * as doorBellAnimation from "./resources/lottieFiles/doorbell.json";
import { getSelectedFamilyToVisit, enableErrorLog } from "./tools/funcs";
import {
  authenticate,
  getStatus,
  familyAdminToken,
  searchFamilyById,
} from "./api";

import "./styles/main.scss";

enableErrorLog();
// import UploadFile from "./components/UploadFileIcon";
export default class LottieControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lottiePlayerRef: null,
      page: {},
      pageVisible: false,
      lottieRunnerKey: null,
      from: 0,
      to: null,
      direction: null,
      backTo: null,
      isPaused: true,
      isStopped: true,
      autoplay: false,
      loop: false,
      width: null,
      height: null,
      introHotSelectedId: null,
      animationData: null,
      backWithoutAnimation: true,
      onlyRender: false,
    };
  }

  updateDimensions = () => {
    console.log("update dimensions");
    // this.setState({
    //   width: window.innerWidth,
    //   height: window.innerHeight
    // });
    // this.setState({
    //   width: "100%",
    //   height: "100%"
    // });
  };

  async componentDidMount() {
    console.log("componentDidMount");
    window.addEventListener("resize", this.updateDimensions);
    this.updateDimensions();

    let page;
    let defaultPage = pages.splash;

    if (store.get("refreshToken")) {
      let pageName = defaultPage.name;
      const lastOpenedPage = store.get("lastOpenedPage");
      if (lastOpenedPage && pages[lastOpenedPage]) pageName = lastOpenedPage;
      page = pages[pageName];
      switch (page) {
        case pages.didemoon:
        case pages.bazdidemoon:
          page = pages.home;
          break;
        case pages.phoneVerify:
          page = pages.phoneOtp;
          break;
        default:
      }
      try {
        await authenticate();
        await getStatus();
        await searchFamilyById(getSelectedFamilyToVisit());
        // await familyAdminToken();
      } catch (error) {}
    } else {
      page = defaultPage;
    }
    this.gotoPage({ page });
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
  }

  runTransition = (transition) => {
    console.log("run transition", transition);
    // console.log(transition);
    // console.log(this.state);
    let newKey = Math.random();
    let direction = transition.direction;
    if (!direction) {
      direction =
        transition.from === undefined ||
        transition.to === undefined ||
        transition.from < transition.to
          ? 1
          : -1;
    }
    console.log(direction);
    while (newKey === this.state.lottieRunnerKey) newKey = Math.random();
    this.setState({
      animationData: transition.animationData,
      callbacks: transition.callbacks,
      from: transition.from,
      to: transition.to,
      direction,
      speed: transition.speed || 1,
      lottieRunnerKey: newKey,
      isPaused: false,
      isStopped: false,
      autoplay: true,
    });
    // if (this.state.onlyRender) {
    //   console.log(this.state.lottiePlayerRef.current.getDuration());
    // }
  };
  back = () => {
    console.log("backTo", this.state.backTo);
    const backTo = store.get("backTo") || pages.home;
    if (backTo) {
      this.gotoPage({
        page: backTo,
        onlyRender: this.state.backWithoutAnimation,
      });
    }
    store.remove("backTo");
  };
  gotoPage = ({
    page,
    backTo = null,
    backWithoutAnimation = true,
    onlyRender = false,
  }) => {
    console.log("gotopage", page, onlyRender);
    let pageVisible = true;
    const pagesVisibleAtEnd = [
      pages.doorBell,
      pages.axamoon,
      pages.taghvimemoon,
      pages.ashpazkhunamoon,
      pages.honarhamoon,
      pages.khaterehamoon,
      pages.panjerehamoon,
      pages.ketabkhunamoon,
      pages.mehmooniamoon,
      pages.chamedoon,
      pages.ahangamoon,
      pages.khodemuniamoon,
      pages.movieLovee,
    ];
    pagesVisibleAtEnd.forEach((p) => {
      if (p.name === page.name) pageVisible = false;
    });
    this.setState({
      page,
      backTo,
      backWithoutAnimation,
      onlyRender,
      pageVisible,
    });
    store.set("lastOpenedPage", page.name);
    if (backTo) store.set("backTo", backTo);
  };

  render() {
    console.log("render", this.state);
    return (
      <div
        className={`appContainer ${this.state.page.name}`}
        onClick={() => {}}
        style={{ height: this.state.height }}
      >
        {this.state.animationData && (
          <SimpleLottieRunner
            animationData={this.state.animationData}
            onComplete={() => {
              if (!this.state.pageVisible)
                this.setState({
                  pageVisible: true,
                });
            }}
            onlyRender={this.state.onlyRender}
            callbacks={this.state.callbacks}
            from={this.state.from}
            to={this.state.to}
            direction={this.state.direction}
            key={this.state.lottieRunnerKey}
            isPaused={this.state.isPaused}
            isStopped={this.state.isStopped}
            autoplay
            loop={this.state.loop}
            speed={this.state.speed}
            width={this.state.width}
            height={this.state.height}
          />
        )}
        <div
          className="fixedPage"
          style={{
            width: this.state.width,
            height: this.state.height,
            visibility: this.state.pageVisible ? "visible" : "hidden",
          }}
        >
          {this.state.page.name === pages.splash.name && (
            <Splash
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.phoneOtp.name && (
            <OTPPhone
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.phoneVerify.name && (
            <OTPVerify
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.intro.name && (
            <Intro
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
              onlyRender={this.state.onlyRender}
            />
          )}
          {this.state.page.name === "doorBell" && (
            <DoorBell
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === "home" && (
            <Home
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.shabamoon.name && (
            <Shabamoon
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === "chalenjamoon" && (
            <Chalenjamoon
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.ahangamoon.name && (
            <Ahangamoon
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {(this.state.page.name === pages.axamoon.name ||
            this.state.page.name === pages.taghvimemoon.name ||
            this.state.page.name === pages.ashpazkhunamoon.name ||
            this.state.page.name === pages.honarhamoon.name ||
            this.state.page.name === pages.khaterehamoon.name ||
            this.state.page.name === pages.panjerehamoon.name ||
            this.state.page.name === pages.ketabkhunamoon.name ||
            this.state.page.name === pages.mehmooniamoon.name ||
            this.state.page.name === pages.chamedoon.name) && (
            <Axamoon
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.filmamoon.name && (
            <Filmamoon
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.khodemuniamoon.name && (
            <Khodemooniamoon
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.movieLovee.name && (
            <MovieLovee
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.baziamoon.name && (
            <Baziamoon
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.search.name && (
            <Search
              onItemSelected={() => {
                this.gotoPage({ page: pages.doorBell });
                // console.log("on item selected");
                // const t = transitions.doorBell;
                // t.callbacks = [
                //   {
                //     event: "onComplete",
                //     run: () => {
                //       console.log("search transition on complete");
                //       // this.gotoPage({ page: pages.home });
                //     },
                //   },
                // ];
                // this.runTransition(t);
              }}
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.khunatoBesaz.name && (
            <KhunatoBesaz
              onClose={() => {
                // if (this.state.backTo) this.gotoPage(pages[this.state.backTo]);
                // else this.gotoPage(pages.home);
                this.gotoPage({ page: pages.intro });
              }}
              onItemSelected={() => {
                this.gotoPage({ page: pages["doorBellDisplayGallery"] });
              }}
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.didemoon.name && (
            <DidemoonPage
              type="didemoon"
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.bazdidemoon.name && (
            <DidemoonPage
              type="bazdidemoon"
              onClose={() => {
                this.gotoPage(pages.intro);
              }}
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
          {this.state.page.name === pages.invitationRequests.name && (
            <InvitationRequests
              onClose={() => {
                this.gotoPage({ page: this.state.backTo || pages.intro });
              }}
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}

          {this.state.page.name === pages.inviteOthers.name && (
            <InviteOthers
              onClose={() => {
                this.gotoPage(pages.home);
              }}
              state={this.state}
              setState={(state) => {
                this.setState(state);
              }}
              runTransition={this.runTransition}
              gotoPage={this.gotoPage}
              back={this.back}
              page={this.state.page}
            />
          )}
        </div>
      </div>
    );
  }
}
