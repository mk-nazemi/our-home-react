import * as splashAnimationData from "./resources/lottieFiles/pages/1-khoonamoon-intro.json";
import * as phoneOTPAnimationData from "./resources/lottieFiles/pages/2-phoneOtp.json";
import * as phoneVerifyAnimationData from "./resources/lottieFiles/pages/3-phoneVerify.json";
// import * as introAnimationData from "./resources/lottieFiles/pages/4-intro.json";
import * as doorBellAnimationData from "./resources/lottieFiles/pages/5-doorBell.json";
import * as homeAnimationData from "./resources/lottieFiles/pages/6-home.json";
import * as baziamoonAnimationData from "./resources/lottieFiles/pages/15-baziamoon.json";
import * as axamoonAnimationData from "./resources/lottieFiles/pages/10 aksamoon.json";
import * as ahangamoonAnimationData from "./resources/lottieFiles/pages/9-ahangamoon.json";
import * as filmamoonAnimationData from "./resources/lottieFiles/pages/11-filmamoon.json";
import * as khodemuniamoonAnimationData from "./resources/lottieFiles/pages/13-khodemooniamoon.json";
import * as movieLoveeAnimationData from "./resources/lottieFiles/pages/14-movielovie.json";
import * as taghvimemoonAnimationData from "./resources/lottieFiles/pages/10 aksamoon.json";
import * as honarhamoonAnimationData from "./resources/lottieFiles/pages/10 aksamoon.json";
import * as ketabkhunamoonAnimationData from "./resources/lottieFiles/pages/10 aksamoon.json";
import * as khaterehamoonAnimationData from "./resources/lottieFiles/pages/10 aksamoon.json";
import * as panjerehamoonAnimationData from "./resources/lottieFiles/pages/10 aksamoon.json";
import * as shabamoonAnimationData from "./resources/lottieFiles/pages/13-khodemooniamoon.json";
import * as mehmooniamoonAnimationData from "./resources/lottieFiles/pages/10 aksamoon.json";
import * as chamedoonAnimationData from "./resources/lottieFiles/pages/10 aksamoon.json";

// family members lottie files
import * as familyMembersBox1 from "./resources/lottieFiles/familyMembersBox/1 nafare.json";
import * as familyMembersBox2 from "./resources/lottieFiles/familyMembersBox/2 nafare.json";
import * as familyMembersBox3 from "./resources/lottieFiles/familyMembersBox/3 nafare.json";
import * as familyMembersBox4 from "./resources/lottieFiles/familyMembersBox/4 nafare.json";
import * as familyMembersBox5 from "./resources/lottieFiles/familyMembersBox/5 nafare.json";
import * as familyMembersBox6 from "./resources/lottieFiles/familyMembersBox/6 nafare.json";
import * as familyMembersBox7 from "./resources/lottieFiles/familyMembersBox/7 nafare.json";
import * as familyMembersBox8 from "./resources/lottieFiles/familyMembersBox/8 nafare.json";
import * as familyMembersBox9 from "./resources/lottieFiles/familyMembersBox/9 nafare.json";
import * as familyMembersBox10 from "./resources/lottieFiles/familyMembersBox/10 nafare.json";

// upload file lottie files
import * as uploadSelectFile from "./resources/lottieFiles/components/upload/selectFile.json";
import * as uploadUploading from "./resources/lottieFiles/components/upload/uploading.json";
import * as uploadUploadSucceed from "./resources/lottieFiles/components/upload/uploadSucceed.json";
import * as uploadUploadFailed from "./resources/lottieFiles/components/upload/uploadFailed.json";

// intro page lottie files
import * as introTopAnimationData from "./resources/lottieFiles/components/intro-top.json";
import * as explorerAnimationData from "./resources/lottieFiles/components/8 box ba atish.json";
// import * as uploadUploadSucceed from "./resources/lottieFiles/components/upload/uploadSucceed.json";
// import * as uploadUploadFailed from "./resources/lottieFiles/components/upload/uploadFailed.json";

// accessArea lottie files
import * as accessAreaAnimationData from "./resources/lottieFiles/components/accessArea.json";

const resourcesPath = "./resources";
export const pagesLottiePath = `${resourcesPath}/lottieFiles/pages`;

export const pages = {
  splash: {
    name: "splash",
  },
  phoneOtp: {
    name: "phoneOtp",
  },
  phoneVerify: {
    name: "phoneVerify",
  },
  intro: {
    name: "intro",
  },
  doorBell: {
    name: "doorBell",
  },
  home: {
    name: "home",
  },
  baziamoon: {
    name: "baziamoon",
  },
  doorBellDisplayGallery: {
    name: "doorBell",
  },
  doorBellStep2: {
    name: "doorBell",
  },
  shabamoon: {
    name: "shabamoon",
    title: "شبامون",
    uploadFileType: "Video",
    validTypes: "video/*",
  },
  ahangamoon: {
    name: "ahangamoon",
    title: "آهنگامون",
    uploadFileType: "Sound",
    validTypes: "audio/*",
  },
  axamoon: {
    name: "axamoon",
    title: "عکسامون",
    uploadFileType: "Image",
    validTypes: "image/*",
  },
  taghvimemoon: {
    name: "taghvimemoon",
    title: "تقویممون",
    uploadFileType: "Image",
    validTypes: "image/*",
  },
  ashpazkhunamoon: {
    name: "ashpazkhunamoon",
    title: "آشپزخونمون",
    uploadFileType: "Image",
    validTypes: "image/*",
  },
  ketabkhunamoon: {
    name: "ketabkhunamoon",
    title: "کتابخونمون",
    uploadFileType: "Image",
    validTypes: "image/*",
  },
  honarhamoon: {
    name: "honarhamoon",
    title: "هنرهامون",
    uploadFileType: "Image",
    validTypes: "image/*",
  },
  panjerehamoon: {
    name: "panjerehamoon",
    title: "پنجره هامون",
    uploadFileType: "Image",
    validTypes: "image/*",
  },
  khaterehamoon: {
    name: "khaterehamoon",
    title: "خاطره هامون",
    uploadFileType: "Image",
    validTypes: "image/*",
  },
  mehmooniamoon: {
    name: "mehmooniamoon",
    title: "مهمونی هامون",
    uploadFileType: "Image",
    validTypes: "image/*",
  },
  chamedoon: {
    name: "chamedoon",
    title: "چمدون",
    uploadFileType: "Image",
    validTypes: "image/*",
  },
  filmamoon: {
    name: "filmamoon",
  },
  khodemuniamoon: {
    name: "khodemuniamoon",
    title: "خودمونیامون",
    uploadFileType: "Video",
    validTypes: "video/*",
  },
  movieLovee: {
    name: "movieLovee",
    title: "مووی لاوی",
    uploadFileType: "Video",
    validTypes: "video/*",
  },
  search: {
    name: "search",
  },
  khunatoBesaz: {
    name: "khunatoBesaz",
  },
  didemoon: {
    name: "didemoon",
  },
  bazdidemoon: {
    name: "bazdidemoon",
  },
  invitationRequests: {
    name: "invitationRequests",
  },
  uploadFile: {
    name: "uploadFile",
  },
  inviteOthers: {
    name: "inviteOthers",
  },
};

export const transitions = {
  splash: {
    animationData: splashAnimationData,
  },
  phoneOtp: {
    animationData: phoneOTPAnimationData,
  },
  phoneVerify: {
    animationData: phoneVerifyAnimationData,
  },
  intro: {
    animationData: introTopAnimationData,
  },
  explorer: {
    animationData: explorerAnimationData,
  },
  doorBell: {
    animationData: doorBellAnimationData,
  },
  home: {
    animationData: homeAnimationData,
    from: 0,
    // to: 72
  },
  homeToBaziamoonIconReverse: {
    animationData: homeAnimationData,
    // from: 72,
    direction: -1,
    to: 10,
  },
  baziamoon: {
    animationData: baziamoonAnimationData,
  },
  homeToAxamoonIconReverse: {
    animationData: homeAnimationData,
    // from: 72,
    direction: -1,
    to: 40,
  },
  axamoon: {
    animationData: axamoonAnimationData,
  },
  homeToAhangamoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  ahangamoon: {
    animationData: ahangamoonAnimationData,
  },
  homeToFilmamoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  filmamoon: {
    animationData: filmamoonAnimationData,
  },
  homeToKhodemuniamoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  khodemuniamoon: {
    animationData: khodemuniamoonAnimationData,
  },

  homeToMovieLoveeIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  movieLovee: {
    animationData: movieLoveeAnimationData,
  },
  homeToTaghvimemoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  taghvimemoon: {
    animationData: taghvimemoonAnimationData,
  },
  homeToHonarhamoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  honarhamoon: {
    animationData: honarhamoonAnimationData,
  },
  homeToKetabkhunamoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  ketabkhunamoon: {
    animationData: ketabkhunamoonAnimationData,
  },
  homeToKhaterehamoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  khaterehamoon: {
    animationData: khaterehamoonAnimationData,
  },
  homeToPanjerehamoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  panjerehamoon: {
    animationData: panjerehamoonAnimationData,
  },
  homeToShabamoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  shabamoon: {
    animationData: shabamoonAnimationData,
  },
  homeToMehmooniamoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  mehmooniamoon: {
    animationData: mehmooniamoonAnimationData,
  },
  homeToChamedoonIconReverse: {
    animationData: homeAnimationData,
    direction: -1,
    to: 40,
  },
  chamedoon: {
    animationData: chamedoonAnimationData,
  },
};

export const familyMembersTransitions = [
  familyMembersBox1,
  familyMembersBox2,
  familyMembersBox3,
  familyMembersBox4,
  familyMembersBox5,
  familyMembersBox6,
  familyMembersBox7,
  familyMembersBox8,
  familyMembersBox9,
  familyMembersBox10,
];

export const UploadTransitions = {
  selectFile: {
    animationData: uploadSelectFile,
  },
  uploading: {
    animationData: uploadUploading,
  },
  succeed: {
    animationData: uploadUploadSucceed,
  },
  failed: {
    animationData: uploadUploadFailed,
  },
};

export const introPageTransitions = {
  top: {
    animationData: "",
  },
  explorer: {
    animationData: "",
  },
};

export const accessArea = {
  animation: {
    animationData: accessAreaAnimationData,
    to: 135,
  },
};
export const UploadTransitions2 = {
  "start-to-upload": {
    id: "u1",
    from: 90,
    to: 178,
    PageOnEnd: pages.home.name,
    speed: 1,
  },
  uploadingPleaseWait: {
    id: "u2",
    from: 474,
    to: 559,
    speed: 1,
  },
  uploadCompleteTransition: {
    id: "u3",
    from: 1134,
    to: 1251,
    // forceRun: true
  },
  uploadFailedTransition: {
    id: "u4",
    from: 1462,
    to: 1637,
  },
};

export const strings = {
  REGISTER_DONE_SUCCESSFULLY: "ثبت نام با موفقیت انچام شد",
  REGISTER_FAILED: "در انجام ثبت نام خطایی رخ داده لطفا مجدد تلاش کنید",
  VIEW_FAMILY_NEED_PERMISSION_DO_YOU_WANT_TO_SEND_REQUEST:
    "برای دیدن محتوای خصوصی باید درخواست خود را ارسال کنید",
  VIEW_FAMILY_PERMISSION_DENIED_NEEDS_YOUR_FAMILY_CREATOR_SEND_REQUEST:
    " مشاهده این قمست امکان پذیر نیست",
  JOIN_TO_FAMILY_REQUEST_FROM: "درخواست عضو شدن در خانواده از طرف ",
  YOUR_ROLE_IN_FAMILY: "نقش شما در خانواده ",
  YOUR_FULL_NAME: "نام کامل شما",
  INVALID_REFERRAL_OTP_ERROR: "هنوز به خونمون دعوت نشدی!",
  REGISTER_YOUR_FAMILY_FIRST: "خونتو هنوز نساختی. تیک سبز رو بزن",
  DELETE_ARE_YOU_SURE: "آیا از حذف مطمئن هستید؟",
  UPLOAD_PERMISSION_DENIED: "دسترسی به آپلود رو نداری",
  DONE_SUCCESSFULLY: "با موفقیت انجام شد",
  captureImage: "انتخاب تصویر",
};

export const RELATIONS = [
  "پدر",
  "مادر",
  "فرزند پسر",
  "فرزند دختر",
  "همسر",
  "پدربزرگ",
  "مادربزرگ",
  "دوست",
  "تنهام",
];

export const VIEW_ACCESSES = [
  {
    label: "عمومی",
    value: "public",
  },
  {
    label: "خصوصی",
    value: "private",
  },
];

export const PERMISSION_LEVELS = {
  VIEW: "View",
  UPLOAD: "Upload",
  ADMIN: "Admin",
};

export const userSections = [];
