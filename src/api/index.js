import {
  parseArabic,
  getMyFirstFamilyId,
  saveMyStatus,
  setSelectedFamilyToVisitInfo,
} from "../tools/funcs";
import { loadProgressBar } from "axios-progress-bar";
import { endpoint } from "../config";
import jwtDecode from "jwt-decode";
const axios = require("axios");
const store = require("store");
// const endpoint = "http://185.236.37.109:14133";
// const endpoint = "https://api-khunamoon.mabeyn.ir";
loadProgressBar();

export async function requestOtp(countryCode, phoneNumber, referral) {
  try {
    // number = parseArabic(number);
    const body = {
      countryCode,
      phoneNumber,
    };
    if (referral) {
      body.referral = referral;
    }
    const res = await axios.post(`${endpoint}/requestOtp`, body);
    console.log(res);
    if (res.data.status === "OK") {
      store.set("otpToken", res.data.otpToken);
      return Promise.resolve();
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function submitOtp(code) {
  try {
    code = parseArabic(code);
    const res = await axios.post(`${endpoint}/submitOtp`, {
      otpToken: store.get("otpToken"),
      otp: code,
    });
    console.log(res);
    if (res.data.status === "OK") {
      store.set("refreshToken", res.data.refreshToken);
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function authenticate() {
  try {
    const userToken = store.get("userToken");

    if (userToken) {
      try {
        const decoded = jwtDecode(userToken);
        // console.log("decoded", decoded);
        if (decoded.exp * 1000 > Date.now()) return;
        if (new Date().getTime() < decoded.exp * 1000) return;
      } catch (error) {
        console.error(error);
        store.remove("userToken");
      }
    }
    const res = await axios.post(`${endpoint}/authenticate`, {
      refreshToken: store.get("refreshToken"),
    });
    console.log(res);
    if (res.data.status === "OK") {
      store.set("userToken", res.data.userToken);

      return Promise.resolve();
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function familyAdminToken(familyId = getMyFirstFamilyId()) {
  try {
    const familyAdminToken = store.get("familyAdminToken");

    if (familyAdminToken) {
      try {
        const decoded = jwtDecode(familyAdminToken);
        // console.log("decoded", decoded);
        if (new Date().getTime() < decoded.exp * 1000) return;
      } catch (error) {
        store.remove("familyAdminToken");
        console.error(error);
      }
    }
    const res = await axios.get(`${endpoint}/family/${familyId}/edit`, {
      headers: { userToken: store.get("userToken") },
    });
    console.log(res);
    if (res.data.status === "OK") {
      store.set("familyAdminToken", res.data.familyAdminToken);

      return Promise.resolve();
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function getFile(fileId) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/file/${fileId}`,
      {},
      { headers: { usertoken: store.get("userToken") } }
    );
    console.log(res);
    if (res.data.status === "OK") {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function deleteFile(fileId) {
  try {
    await authenticate();
    const res = await axios.delete(`${endpoint}/file/${fileId}`, {
      headers: { usertoken: store.get("userToken") },
    });
    console.log(res);
    if (res.data.status === "OK") {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function likeFile(fileId) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/file/${fileId}/like`,
      {},
      { headers: { usertoken: store.get("userToken") } }
    );
    console.log(res);
    if (res.data.status === "OK") {
      return Promise.resolve();
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function unlikeFile(fileId) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/file/${fileId}/unlike`,
      {},
      { headers: { usertoken: store.get("userToken") } }
    );
    console.log(res);
    if (res.data.status === "OK") {
      return Promise.resolve();
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function esfandFile(fileId) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/file/${fileId}/esfand`,
      {},
      { headers: { usertoken: store.get("userToken") } }
    );
    console.log(res);
    if (res.data.status === "OK") {
      return Promise.resolve();
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function seenFile(fileId) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/file/${fileId}/seen`,
      {},
      { headers: { usertoken: store.get("userToken") } }
    );
    console.log(res);
    if (res.data.status === "OK") {
      return Promise.resolve();
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function getUploadToken() {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/challenge/upload`,
      { isWeb: typeof androidApp === "undefined" },
      { headers: { usertoken: store.get("userToken") } }
    );
    console.log(res);
    if (res.data.status === "OK") {
      return Promise.resolve(res.data.uploadToken);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function createFamily(
  firstname,
  lastname,
  role,
  familyTitle,
  referral
) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/family`,
      { firstname, lastname, role, familyTitle, referral },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (res.data.status === "OK") {
      store.set("refreshToken", res.data.newRefreshToken);
      store.set("userToken", res.data.newAccessToken);
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function addMember(
  familyId,
  inviteeRole,
  inviteePhoneNumber,
  inviteeFirstname,
  inviteeLastname
) {
  try {
    await authenticate();
    await familyAdminToken(familyId);
    const res = await axios.post(
      `${endpoint}/family/member`,
      {
        inviteeRole,
        inviteePhoneNumber,
        inviteeFirstname,
        inviteeLastname,
        familyAdminToken: store.get("familyAdminToken"),
      },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function getFamilyConnectionRequests() {
  try {
    await authenticate();
    const res = await axios.get(`${endpoint}/family/connectionRequest`, {
      headers: { usertoken: store.get("userToken") },
    });
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data.request);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function familyConnectionRequest(targetFamilyId) {
  try {
    await authenticate();
    await familyAdminToken();
    const res = await axios.post(
      `${endpoint}/family/connectionRequest`,
      {
        targetFamilyId,
        familyAdminToken: store.get("familyAdminToken"),
      },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function familyActionOnConnectionRequest(
  invitationId,
  action,
  defaultPermissions
) {
  try {
    await authenticate();
    await familyAdminToken();
    const res = await axios.post(
      `${endpoint}/family/connectionRequest/${invitationId}`,
      {
        action,
        defaultPermissions,
        familyAdminToken: store.get("familyAdminToken"),
      },
      {
        headers: {
          usertoken: store.get("userToken"),
          "Content-Type": "application/json",
        },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
  // try {
  //   await authenticate();
  //   await familyAdminToken();
  //   var data = `{\n\t"action":"${action}",\n\t"defaultPermissions":"${JSON.stringify(
  //     defaultPermissions
  //   )}",\n\t"familyAdminToken":"${store.get("familyAdminToken")}"\n}`;
  //   var xhr = new XMLHttpRequest();
  //   // xhr.withCredentials = true;
  //   xhr.addEventListener("readystatechange", function() {
  //     if (this.readyState === 4) {
  //       console.log(this.responseText);
  //       if (!this.response.data.status.endsWith("!")) {
  //         return Promise.resolve(this.response.data);
  //       }
  //     }
  //   });
  //   xhr.open("POST", `${endpoint}/connectionRequest/${invitationId}`);
  //   xhr.setRequestHeader("usertoken", store.get("userToken"));
  //   xhr.send(data);
  // } catch (error) {
  //   console.error(error);
  //   return Promise.reject(error);
  // }
}

export async function getStatus() {
  try {
    await authenticate();
    const res = await axios.get(`${endpoint}/status`, {
      headers: { usertoken: store.get("userToken") },
    });
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      const { data } = res;
      saveMyStatus(data);
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function actionOnFamilyInvitation(requestId, action) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/family/request/${requestId}`,
      { action },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function getInviteLink(validNumbers) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/inviteLink`,
      { validNumbers },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function searchUser(query = "", start = 0, limit = 12) {
  try {
    await authenticate();
    const res = await axios.get(
      `${endpoint}/family/search?q=${query}&skip=${start}&limit=${limit}`,
      // `${endpoint}/family/search?q=${query}`,
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function searchFamilyById(id) {
  try {
    await authenticate();
    const res = await axios.get(`${endpoint}/family/search?id=${id}`, {
      headers: { usertoken: store.get("userToken") },
    });
    console.log(res);
    if (
      !res.data.status.endsWith("!") &&
      res.data.searchResults &&
      res.data.searchResults[0]
    ) {
      setSelectedFamilyToVisitInfo(res.data.searchResults[0]);
      return Promise.resolve(res.data.searchResults[0]);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function requestToFamilyForRelation(familyId) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/family/request`,
      { familyId },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function getUploadUrl(fileName, fileType) {
  console.log(fileName, fileType);
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/upload`,
      { fileName, fileType },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}
export async function uploadDone(
  fileId,
  { contentType, familyId, place, type, metadata, videoThumbnail }
) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/upload/finished/${fileId}`,
      { contentType, familyId, place, type, metadata, videoThumbnail },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}
export async function getFamilyFiles() {}
export async function getFiles() {}
export async function getFilesAPI(familyId, place) {
  try {
    await authenticate();
    const res = await axios.get(`${endpoint}/file/${familyId}/${place}`, {
      headers: { usertoken: store.get("userToken") },
    });
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      console.log(res.data);
      let fileIds = res.data.files.map((file) => file._id);
      console.log(fileIds);
      const links = await getFilesLinkBatch(fileIds);
      return Promise.resolve(
        Object.values(links.fileList).filter(
          (file) => Object.keys(file).length > 0
        )
      );

      // let result = [];
      // for (let index = 0; index < res.data.files.length; index++) {
      //   const file = res.data.files[index];
      //   try {
      //     const res = await getFile(file._id);
      //     if (res.status === "OK") {
      //       result.push({
      //         file: res.requestedFile,
      //         likeCount: res.requestedFile
      //       });
      //     }
      //   } catch (error) {
      //     console.log(error);
      //   }
      // }
      // return Promise.resolve(result);
      // return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function getFileLink(fileId) {
  try {
    await authenticate();
    const res = await axios.post(`${endpoint}/file/${fileId}`, {
      headers: { usertoken: store.get("userToken") },
    });
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function getFilesLinkBatch(fileIds) {
  try {
    await authenticate();
    const res = await axios.post(
      `${endpoint}/file/batch`,
      { fileIds },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function getDoorBellConfig(familyId) {
  try {
    await authenticate();
    const res = await axios.get(`${endpoint}/family/${familyId}/doorbell`, {
      headers: { usertoken: store.get("userToken") },
    });
    console.log(res);
    //   {
    //     "status": "OK",
    //     "doorbellConfig": {
    //         "testConfigField": "testConfigValue"
    //     },
    //     "memberIds": [
    //         "5e889c24b2b4380008c71bd5"
    //     ]
    // }
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data.doorbellConfig);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function setDoorBellConfig(doorbellConfig) {
  try {
    await authenticate();
    await familyAdminToken();
    const res = await axios.post(
      `${endpoint}/family/doorbell`,
      { familyAdminToken: store.get("familyAdminToken"), doorbellConfig },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data.doorbellConfig);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function setMemberProfile({
  memberUserId,
  profileImage,
  setUploadPercent,
}) {
  try {
    await authenticate();
    await familyAdminToken();
    const res = await axios.post(
      `${endpoint}/family/setMemberProfile`,
      {
        familyAdminToken: store.get("familyAdminToken"),
        memberUserId,
        profileImage,
      },
      {
        onUploadProgress: (progressEvent) => {
          const totalLength = progressEvent.lengthComputable
            ? progressEvent.total
            : progressEvent.target.getResponseHeader("content-length") ||
              progressEvent.target.getResponseHeader(
                "x-decompressed-content-length"
              );
          console.log("onUploadProgress", totalLength);
          if (totalLength !== null) {
            const percent = Math.floor(
              (progressEvent.loaded * 100) / totalLength
            );
            console.log(Math.floor((progressEvent.loaded * 100) / totalLength));
            typeof setUploadPercent === "function" && setUploadPercent(percent);
          }
        },
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function grantAccess(targetUserId, permissions) {
  try {
    await authenticate();
    await familyAdminToken();
    const res = await axios.post(
      `${endpoint}/family/grantAccess`,
      {
        familyAdminToken: store.get("familyAdminToken"),
        targetUserId,
        permissions,
      },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function getFamilyPermissions() {
  try {
    await authenticate();
    await familyAdminToken();
    const res = await axios.post(
      `${endpoint}/family/permissions`,
      {
        familyAdminToken: store.get("familyAdminToken"),
      },
      {
        headers: { usertoken: store.get("userToken") },
      }
    );
    console.log(res);
    if (!res.data.status.endsWith("!")) {
      return Promise.resolve(res.data.accessObject);
    }
    return Promise.reject(res.data.status);
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
}

export async function logError(error) {
  try {
    const res = await axios.post(
      `https://api-rubika-ajimaji.serveapp.ir/error`,
      { error }
    );
    return Promise.resolve();
  } catch (error) {
    return Promise.reject(error);
  }
}
