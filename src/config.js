const RUN_MODES = {
  DEBUG: "DEBUG",
  PRODUCTION: "PRODUCTION",
};
const runMode = RUN_MODES.PRODUCTION;

let endpoint;
switch (runMode) {
  case RUN_MODES.DEBUG:
    endpoint = "https://api-dev-khunamoon.mabeyn.ir";
    break;
  case RUN_MODES.PRODUCTION:
    endpoint = "https://api-khunamoon.mabeyn.ir";
    break;
  default:
    endpoint = "https://api-khunamoon.mabeyn.ir";
}

const USER_PROFILE_PIC_URL_TEMPLATE = `https://fs.shegeftangizan.com/familyId/memberId.jpg?d=random`;
module.exports = {
  RUN_MODES,
  runMode,
  endpoint,
  USER_PROFILE_PIC_URL_TEMPLATE,
};
